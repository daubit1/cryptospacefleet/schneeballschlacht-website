import { test as base, expect, BrowserContext, chromium } from "@playwright/test";
import path from "path";

export const test = base.extend<{
  context: BrowserContext;
  extensionId: string;
}>({
  context: async ({ }, use) => {
    const pathToExtension = path.join(__dirname, 'my-extension/metamask');
    const userDataDir = '/userdir';
    console.log(userDataDir)
    const context = await chromium.launchPersistentContext(userDataDir, {
      headless: false,
      args: [
        `--disable-extensions-except=${pathToExtension}`,
        `--load-extension=${pathToExtension}`,
      ],
    });
    await use(context);
    await context.close();
  },
  extensionId: async ({ context }, use) => {
    let [background] = context.serviceWorkers();
    if (!background)
      background = await context.waitForEvent("serviceworker");

    const extensionId = background.url().split("/")[2];
    await use(extensionId);
  },
});

test("metamask login/logout", async ({ page, extensionId }) => {
  await page.goto(`chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#unlock`);
  // await page.goto(`chrome-extension://hnfanknocfeofbddgcijnmhnfnkdnaad/index.html?launchedForOnboarding=true`);
  await page.waitForTimeout(3000);
  const getInputPassword = page.locator('input')
  await expect(page.locator('text=Welcome Back!')).toBeVisible();
  await getInputPassword.fill("password!")
  
  const getUnlock = page.locator('text=Unlock');
  await getUnlock.click()

  await page.goto('http://localhost:3000/');
  await page.waitForTimeout(3000);
  await expect(page).toHaveTitle(/Create Next App/);
  await expect(page.locator('text=schneeball')).toBeVisible();
  await expect(page.locator('text=schlacht')).toBeVisible();

  const getLogin = page.locator('text=Login');
  await getLogin.click();
  const getMetamask = page.locator('text=Metamask');
  await getMetamask.click();
  await page.waitForTimeout(3000);

  const getLogout = page.locator('#logout');
  await getLogout.click();

  await page.close();
});
