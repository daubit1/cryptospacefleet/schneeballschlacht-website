# Schneeballschlacht

This repository contains the frontend of the Schneeballschlacht smart contracts. <br>
Schneeballschlacht is a DAG (Decentralized Autonomous Game). It was designed to be deployed and played automomously.
The game is played in rounds which can start and end by anybody. <br>


## Development

The following assumes the use of `node@>=16`

### Install dependecies

`yarn`

### Run locally

`yarn dev`

## CI/CD
We are utilizing [rust-script](https://rust-script.org/) to write scripts in Rust.
Install rust-script to use the script. The script folder contains a few more files.
[Akash](akash.network) is used to host the frontend. One can either use the CLI to deploy or the GUI.
Deploying the frontend generates a URI which needs to be passed to `upload-cloudflare.mjs`. 
Scripts for akash were in development but arent mainly used. The same goes for `upload-skynet.js`

- `build.script.rs`
    - Builds and pushes with docker-compose
    - Updates version in `package.json` and `deploy.yaml`
    - tags, commits, pushes git repository
- `upload-cloudflare.mjs` is used to set the CNAME record of a domain.<br>
    It expects one argument which should be the URI of the deployment on Akash.
- `deploy.yaml` contains metadata for Akash deployment.

