import { Data, DataSnowball } from "../types/Data";
import { DataImages } from "../types/DataImages";
import { parseEther } from "ethers/lib/utils";

export const CONTRACT_ADDR = "0xd0075C50C454BFA48927524F708b532442706501";
export const MINT_FEE = parseEther("0.05");
export const TOSS_FEE = parseEther("0.01");

export const Users: Data[] = [
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    balls: 150,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 15,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 18,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    balls: 150,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 15,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 18,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    balls: 150,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 20,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 19,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    balls: 150,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 15,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 18,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 6,
    balls: 150,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 5,
    balls: 250,
  },
  {
    address: "1234567890qwertzuiop0987654321abc",
    level: 2,
    balls: 250,
  },
];

export const Snowballs: DataSnowball[] = [
  {
    id: 1,
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    minlevel: 7,
  },
  {
    id: 2,
    address: "1234567890qwertzuiop0987654321abc",
    level: 15,
    minlevel: 11,
  },
  {
    id: 3,
    address: "1234567890qwertzuiop0987654321abc",
    level: 18,
    minlevel: 14,
  },
  {
    id: 4,
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    minlevel: 4,
  },
  {
    id: 5,
    address: "1234567890qwertzuiop0987654321abc",
    level: 15,
    minlevel: 14,
  },
  {
    id: 6,
    address: "1234567890qwertzuiop0987654321abc",
    level: 18,
    minlevel: 14,
  },
  {
    id: 7,
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    minlevel: 8,
  },
  {
    id: 8,
    address: "1234567890qwertzuiop0987654321abc",
    level: 20,
    minlevel: 14,
  },
  {
    id: 9,
    address: "1234567890qwertzuiop0987654321abc",
    level: 19,
    minlevel: 14,
  },
  {
    id: 10,
    address: "1234567890qwertzuiop0987654321abc",
    level: 10,
    minlevel: 5,
  },
  {
    id: 11,
    address: "1234567890qwertzuiop0987654321abc",
    level: 15,
    minlevel: 4,
  },
  {
    id: 12,
    address: "1234567890qwertzuiop0987654321abc",
    level: 18,
    minlevel: 14,
  },
  {
    id: 13,
    address: "1234567890qwertzuiop0987654321abc",
    level: 6,
    minlevel: 1,
  },
  {
    id: 14,
    address: "1234567890qwertzuiop0987654321abc",
    level: 5,
    minlevel: 3,
  },
  {
    id: 15,
    address: "1234567890qwertzuiop0987654321abc",
    level: 2,
    minlevel: 1,
  },
];

export const images: DataImages[] = [
  {
    id: 1,
    image: "/images/LVL01.png",
  },
  {
    id: 2,
    image: "/images/LVL02.png",
  },
  {
    id: 3,
    image: "/images/LVL03.png",
  },
  {
    id: 4,
    image: "/images/LVL04.png",
  },
  {
    id: 5,
    image: "/images/LVL05.png",
  },
  {
    id: 6,
    image: "/images/LVL06.png",
  },
  {
    id: 7,
    image: "/images/LVL07.png",
  },
  {
    id: 8,
    image: "/images/LVL08.png",
  },
  {
    id: 9,
    image: "/images/LVL09.png",
  },
  {
    id: 10,
    image: "/images/LVL10.png",
  },
  {
    id: 11,
    image: "/images/LVL11.png",
  },
  {
    id: 12,
    image: "/images/LVL12.png",
  },
  {
    id: 13,
    image: "/images/LVL13.png",
  },
  {
    id: 14,
    image: "/images/LVL14.png",
  },
  {
    id: 15,
    image: "/images/LVL15.png",
  },
  {
    id: 16,
    image: "/images/LVL16.png",
  },
  {
    id: 17,
    image: "/images/LVL17.png",
  },
  {
    id: 18,
    image: "/images/LVL18.png",
  },
  {
    id: 19,
    image: "/images/LVL19.png",
  },
  {
    id: 20,
    image: "/images/Gewinner_NFT.jpg",
  },
];
