import retry from "async-retry";
import chains from "../assets/chains.json";
import { Network } from "./network";

export const formatNumber = (n: number) => {
  if (n >= 1e18) {
    return (n / 1e18).toFixed(1).replace(/\.0$/, "") + "Q";
  }
  if (n >= 1e15) {
    return (n / 1e15).toFixed(1).replace(/\.0$/, "") + "q";
  }
  if (n >= 1e12) {
    return (n / 1e12).toFixed(1).replace(/\.0$/, "") + "T";
  }
  if (n >= 1e9) {
    return (n / 1e9).toFixed(1).replace(/\.0$/, "") + "B";
  }
  if (n >= 1e6) {
    return (n / 1000000).toFixed(1).replace(/\.0$/, "") + "M";
  }
  if (n >= 1e3) {
    return (n / 1000).toFixed(1).replace(/\.0$/, "") + "K";
  }
  return n;
};

export async function retryCall<T>(
  promise: Promise<T>,
  log: string = ""
): Promise<T> {
  return await retry(
    async () => {
      if (log) console.log(`Retrying: ${log}`);
      return await promise;
    },
    { retries: 3 }
  );
}

export const toHex = (num: number) => {
  return "0x" + num.toString(16);
};

export function getNetwork(network: Network) {
  const chain = chains[network];
  return {
    chainId: toHex(chain.chainId), // A 0x-prefixed hexadecimal string
    chainName: chain.name,
    nativeCurrency: {
      name: chain.nativeCurrency.name,
      symbol: chain.nativeCurrency.symbol, // 2-6 characters long
      decimals: chain.nativeCurrency.decimals,
    },
    rpcUrls: chain.rpc,
    blockExplorerUrls: [
      chain.explorers && chain.explorers.length > 0 && chain.explorers[0].url
        ? chain.explorers[0].url
        : chain.infoURL,
    ],
  };
}

export function getRPC(network: Network) {
  switch (network) {
    case Network.mumbai:
      return "https://matic-mumbai.chainstacklabs.com";
    case Network.polygon:
      return "https://polygon-rpc.com/";
    case Network.hardhat:
      return "http://127.0.0.1:8545";
    case Network.evmosTestnet:
      return "https://eth.bd.evmos.dev:8545";
  }
}

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
