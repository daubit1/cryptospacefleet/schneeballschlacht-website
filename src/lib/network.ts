export enum Network {
  polygon = 137,
  polygonTestnet = 80001,
  mumbai = 80001,
  hardhat = 31337,
  // ethereum = 1,
  evmosTestnet = 9000,
}

export interface LoginProps {
  network: Network;
}
