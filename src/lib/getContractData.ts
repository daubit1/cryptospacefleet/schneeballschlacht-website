import type { Web3Provider } from "@ethersproject/providers";
import { BigNumber, Contract, ContractReceipt, utils } from "ethers";
import { uniq } from "lodash";
import { setStatsFriendsBoard } from "../state/statsFriendsBoardSlice";
import {
  LeaderBoard,
  setStatsLeaderBoard,
  StatsBoard,
} from "../state/statsLeaderBoardSlice";
import { setStatsSnowballs, StatsSnowball } from "../state/statsSnowballsSlice";
import { setStatsRoundTotal } from "../state/statsTotalRoundSlice";
import { setStatsTotal } from "../state/statsTotalSlice";
import { CONTRACT_ADDR } from "./const";
import { retryCall } from "./functions";

interface QueryToken {
  level: number;
  player: string;
  tokenId: BigNumber;
  hasStone: boolean;
  partnerCount: number;
}

interface Token {
  [address: string]: QueryToken[];
}

function getTotalTosses(
  contract: Contract,
  round: number | "latest"
): Promise<BigNumber> {
  return round === "latest"
    ? retryCall(contract["totalTosses()"]())
    : retryCall(contract["totalTosses(uint256)"](round));
}

function getPayout(
  contract: Contract,
  provider: Web3Provider,
  round: number | "latest",
  global: boolean
): Promise<BigNumber> {
  return round === "latest"
    ? global
      ? retryCall(contract["getPayout()"]())
      : retryCall(provider.getBalance(CONTRACT_ADDR))
    : retryCall(contract["getPayout(uint256)"](round));
}

function getTotalSupply(
  contract: Contract,
  round: number | "latest"
): Promise<BigNumber> {
  return round === "latest"
    ? retryCall(contract["totalSupply()"]())
    : retryCall(contract["totalSupply(uint256)"](round));
}

function getTokens(
  contract: Contract,
  round: number | "latest",
  page: number,
  amount: number
): Promise<QueryToken[]> {
  return round === "latest"
    ? retryCall(contract["getTokens(uint256,uint256)"](page, amount))
    : retryCall(
        contract["getTokens(uint256,uint256,uint256)"](round, page, amount)
      );
}

export const getTotalStatistik = async (
  contract: Contract,
  provider: Web3Provider
) => {
  const currentRound: BigNumber = await contract["getRoundId()"]();
  let totalTosses = 0;
  let totalPayout = BigNumber.from(0);
  for (let i = 1; i <= currentRound.toNumber(); i++) {
    const totalTossesRound: BigNumber = await getTotalTosses(contract, i);
    totalTosses = totalTosses + totalTossesRound.toNumber();
    const totalPayoutRound: BigNumber = await getPayout(
      contract,
      provider,
      i,
      true
    );

    totalPayout = totalPayout.add(totalPayoutRound);
  }
  return setStatsTotal({
    round: currentRound.toNumber(),
    tosses: totalTosses,
    money: +utils.formatEther(totalPayout),
  });
};

export const getRoundStatistik = async (
  contract: Contract,
  provider: Web3Provider,
  account: string | undefined,
  round: number | "latest"
) => {
  const players: string[] = [];
  const tokenIds: number[] = [];
  const stone: number[] = [];
  const level: number[] = [];
  const snowballsTossed: string[] = [];
  const totalCurrentToken: QueryToken[] = [];
  const snowballs: StatsSnowball[] = [];

  const totalTossesRound: BigNumber = await getTotalTosses(contract, round);
  const totalPayoutRound: BigNumber = await getPayout(
    contract,
    provider,
    round,
    false
  );
  const totalSupply: number = (
    await getTotalSupply(contract, round)
  ).toNumber();
  const pages = totalSupply / 500;
  console.log(pages);

  for (let page = 0; page <= pages; page++) {
    const currentToken: QueryToken[] = await getTokens(
      contract,
      round,
      page,
      500
    );
    totalCurrentToken.push(...currentToken);
    console.log(currentToken);
    console.log(account);

    for (const element of currentToken) {
      players.push(element.player);
      tokenIds.push(element.tokenId.toNumber());
      level.push(element.level);

      if (element.player === account) {
        const snowball: StatsSnowball = {
          snowball: element.tokenId.toNumber(),
          levelSnowball: element.level,
          partnerCount: element.partnerCount,
        };
        snowballs.push(snowball);
      }
      if (element.hasStone) {
        stone.push(element.tokenId.toNumber());
      }
      if (element.partnerCount > 0) {
        snowballsTossed.push(element.tokenId.toString());
      }
    }
  }

  const maxLevel = Math.max(...level);
  const totalRoundPlayers = uniq(players).length;

  return {
    totalCurrentToken,
    snowballsTossed,
    statsSnowballs: setStatsSnowballs(snowballs),
    statsRoundTotal: setStatsRoundTotal({
      currentHighestLevel: maxLevel,
      players: totalRoundPlayers,
      stones: stone.length,
      tosses: totalTossesRound.toNumber(),
      money: +utils.formatEther(totalPayoutRound),
      snowballs: tokenIds.length,
    }),
  };
};

export const getLeaderBoard = (
  totalCurrentToken: QueryToken[],
  snowballsTossed: string[],
  account: string | undefined
) => {
  const filteredTokensBySnowballsTossed = totalCurrentToken.filter(
    ({ tokenId }) => snowballsTossed.includes(tokenId.toString())
  );
  let groupByPlayer = filteredTokensBySnowballsTossed.reduce((r: Token, i) => {
    r[i.player] = r[i.player] || [];
    r[i.player].push(i);
    return r;
  }, {});
  let list: LeaderBoard = {
    player: [],
  };
  const accountTokenIds: string[] = [];
  let arrPlayer: StatsBoard[] = [];

  for (const [key, value] of Object.entries(groupByPlayer)) {
    const items = value as QueryToken[];
    const lev = [];
    let snowball = 0;
    if (key === account) {
      for (const item of items) {
        accountTokenIds.push(item.tokenId.toString());
      }
    }
    for (const item of items) {
      lev.push(item.level);
      snowball = snowball + item.partnerCount;
    }
    const playerList = {
      playerBoard: key,
      levelBoard: Math.max(...lev),
      tossedSnowballBoard: snowball,
    };
    arrPlayer.push(playerList);
    list.player.push(playerList);
  }
  console.log("list.player", list);
  return {
    accountTokenIds,
    arrPlayer,
    statsLeaderBoard: setStatsLeaderBoard(list.player),
  };
};

export const getFriendsList = async (
  accountTokenIds: string[],
  contract: Contract,
  totalCurrentToken: QueryToken[],
  arrPlayer: StatsBoard[]
) => {
  const partnersIds: string[] = [];
  for (const partnerIds of accountTokenIds) {
    const getPartnerTokenIds: StatsBoard[] = await retryCall(
      contract["getPartnerTokenIds(uint256)"](partnerIds)
    );
    if (getPartnerTokenIds.length > 1) {
      for (const element of getPartnerTokenIds) {
        partnersIds.push(element.toString());
      }
    } else {
      partnersIds.push(getPartnerTokenIds.toString());
    }
  }

  const filteredPartners = totalCurrentToken.filter(({ tokenId }) =>
    partnersIds.includes(tokenId.toString())
  );

  const partAddress: string[] = [];
  for (const element of filteredPartners) {
    partAddress.push(element.player);
  }

  const uniqPartAddress = uniq(partAddress);
  const arr = [];

  for (const el of uniqPartAddress) {
    for (const [key, value] of Object.entries(arrPlayer)) {
      const v = value as StatsBoard;
      if (v.playerBoard === el) {
        arr.push(value);
      }
    }
  }
  return setStatsFriendsBoard(arr);
};

export const isPaused = (contract: Contract) => {
  return retryCall<boolean>(contract["paused()"]());
};

export const isFinished = (contract: Contract) => {
  return retryCall<boolean>(contract["finished()"]());
};

export function getMintedTokenFromLogs(receipt: ContractReceipt) {
  const logs = receipt.logs;
  const ret = [];
  for (const log of logs) {
    // transfer sig
    if (
      log.topics.length > 0 &&
      log.topics[0] ===
        "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef"
    ) {
      // from 0x0
      if (
        log.topics[1] ===
        "0x0000000000000000000000000000000000000000000000000000000000000000"
      ) {
        // has 0x prefix -> hex is auto detected
        ret.push({
          id: parseInt(log.topics[3]),
          to: stringToAddress(log.topics[2]),
        });
      }
    }
  }
  return ret;
}

export function stringToAddress(addr: string) {
  if (addr[0] === "0" && addr[1] === "x") {
    // last 40 symbols, we add the leading 0x manually
    return `0x${addr.substring(addr.length - 40)}`;
  }
  throw new Error("No a address");
}

export function getLevelupToken(receipt: ContractReceipt) {
  for (const log of receipt.logs) {
    // transfer sig
    if (
      log.topics.length > 0 &&
      log.topics[0] ===
        "0x3b7ca85b5e3d1ebb3a81e8d9e27bfeb9ef2f447026c115421132a20b0733b73f"
    ) {
      return { tokenId: log.topics[2], round: log.topics[1] };
    }
  }
}

export function hasStone(receipt: ContractReceipt) {
  return (
    receipt.logs.find((x) =>
      x.topics.includes(
        // Timeout(address,uint256,address) sig
        "0xc0f855ef2329c4d9465673efb1d4fc16ec975c9ec58340b2358757859fcff970"
      )
    ) !== undefined
  );
}

export async function isPlayer(contract: Contract, addr: string) {
  return (await contract["balanceOf(address)"](addr)).toNumber() > 0;
}
