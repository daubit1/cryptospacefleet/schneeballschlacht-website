export interface Data {
  address: string;
  level: number;
  balls: number;
}

export interface DataSnowball {
  id: number;
  address: string;
  level: number;
  minlevel: number;
}
