export interface DataRule {
  id: number;
  titel: string;
  text1: string;
  text2: string;
}
