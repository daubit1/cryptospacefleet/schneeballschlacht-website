import delay from "lodash/delay";
import { getCookieConsentValue } from "react-cookie-consent";
import ReactGA from "react-ga4";
import { UaEventOptions } from "react-ga4/types/ga4";
import Cookies from "universal-cookie";

abstract class Environment {
  static isProduction =
    typeof window !== "undefined" &&
    !window.location.href.includes("localhost");
}

export enum CookieIDs {
  // Google Analytics
  ga = "_ga",
  gat = "_gat",
  gid = "_gid",
  dcGtm = "_dc_gtm_",
  gac = "_gac_",
  // ampToken = "AMP_TOKEN",
  fpb = "_fbp",
  wd = "wd",
  sb = "sb",
  datr = "datr",
}

export default abstract class GAnalytics {
  static eventBuffer: (() => void)[] = [];
  static didInit = false;
  static initialize(): void {
    const isCookieStatisticsOn = getCookieConsentValue();
    if (isCookieStatisticsOn === "true") {
      ReactGA.initialize([
        { trackingId: "G-3ZQ7YK2G3X", gaOptions: { anonymizeIp: true } },
      ]);
      delay(() => {
        this.didInit = true;
      }, 1000);
    }
    this.clearEventBuffer();
  }

  static event(args: UaEventOptions): void {
    this.sendAnalytics(() => {
      ReactGA.event(args);
    });
  }

  static pageview(path: string): void {
    this.sendAnalytics(() => {
      ReactGA.send({ hitType: "pageview", page: path });
    });
  }

  static removeGACookies(): void {
    const cookies = new Cookies();
    cookies.remove(CookieIDs.ga);
    cookies.remove(CookieIDs.gat);
    cookies.remove(CookieIDs.gid);
    cookies.remove(CookieIDs.dcGtm);
    // cookies.remove(CookieIDs.ampToken);
    cookies.remove(CookieIDs.gac);
    cookies.remove(CookieIDs.fpb);
    cookies.remove(CookieIDs.wd);
    cookies.remove(CookieIDs.sb);
    cookies.remove(CookieIDs.datr);
    cookies.remove("language");
    cookies.remove("currency");
    cookies.remove("_ga_V9S37BCMG9");
    cookies.remove("_ga_S6GW6T5XZV");
  }

  private static sendAnalytics(callback: () => void) {
    const isCookieStatisticsOn = getCookieConsentValue();
    if (
      isCookieStatisticsOn === "true" &&
      Environment.isProduction &&
      this.didInit
    ) {
      callback();
    } else {
      // Limit buffer size
      if (this.eventBuffer.length < 33) {
        this.eventBuffer.push(callback);
      }
    }
  }

  private static clearEventBuffer(): void {
    for (const cb of this.eventBuffer) {
      cb();
    }
    this.eventBuffer = [];
  }
}
