import { useWeb3React } from "@web3-react/core";
import { useTranslation } from "next-i18next";
import useStartRound from "../hooks/useStartRound";
import Image from "next/image";
import { motion } from "framer-motion";
import useEndRound from "../hooks/useEndRound";
import { useSelector } from "react-redux";
import { AppState } from "../state";

const buttonsHover = {
  hover: {
    scale: 1.1,
    transition: {
      duration: 0.3,
    },
  },
};

export default function NoRound() {
  const { i18n, t } = useTranslation();
  const context = useWeb3React();
  const startRound = useStartRound(context.account!);
  const endRound = useEndRound();

  const finished = useSelector((state: AppState) => state.finished.finished);

  return (
    <div className="flex flex-col justify-center">
      {/* <h1 className="statistic textTitel">{t("statistik.titel1")}</h1> */}
      <div className="flex flex-row justify-center">
        <motion.button
          type="button"
          onClick={finished ? endRound : startRound}
          className="relative w-[45%] sm-xs:w-[40%] sm-s:w-[32%] sm:w-[34%] md-l:w-[30%] xl:w-[25%]"
          variants={buttonsHover}
          whileHover="hover"
        >
          <Image
            src="/images/Box_UL.png"
            layout="responsive"
            width={406}
            height={230}
            alt="topL"
          />
          <div className="buttonLClipPatch absolute top-[19%] right-[13%] z-20 flex h-[62%] w-[80%] cursor-pointer items-center justify-center border-none p-0">
            <span className="text-[12px] font-bold text-[#76f3ff] sm-s:text-sm md-l:text-base lg:text-lg 2xl:text-2xl 4K:text-5xl">
              {finished ? t("buttons.end") : t("buttons.start")}
            </span>
          </div>
        </motion.button>
        <motion.button
          type="button"
          className="relative w-[45%] sm-xs:w-[40%] sm-s:w-[32%] sm:w-[34%] md-l:w-[30%] xl:w-[25%]"
          variants={buttonsHover}
          whileHover="hover"
        >
          <a href={`/${i18n.language}/stats`} className="m-2 p-1">
            <Image
              src="/images/Box_UR.png"
              layout="responsive"
              width={406}
              height={230}
              alt="topL"
            />

            <div className="buttonLClipPatch absolute top-[19%] left-[13%] z-20 flex h-[62%] w-[80%] cursor-pointer items-center justify-center border-none p-0">
              <span className="text-[12px] font-bold text-[#76f3ff] sm-s:text-sm md-l:text-base lg:text-lg 2xl:text-2xl 4K:text-5xl">
                {t("buttons.stats")}
              </span>
            </div>
          </a>
        </motion.button>
      </div>
    </div>
  );
}
