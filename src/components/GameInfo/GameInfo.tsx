import { useTranslation } from "next-i18next";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import Circle from "../Circle/Circle";
import { motion } from "framer-motion";
import { formatNumber } from "../../lib/functions";
import { AppState } from "../../state";
import Image from "next/image";

const classNameRounds =
  "flex flex-col xsm:h-[75px] xsm:w-[75px] sm-xs:h-[90px] sm-xs:w-[90px] sm-s:h-[95px] sm-s:w-[95px] md:h-[115px] md:w-[115px] lg:h-[145px] lg:w-[145px] xl:h-[165px] xl:w-[165px] 2xl:h-[215px] 2xl:w-[215px] 3xl:h-[265px] 3xl:w-[265px] 4K:h-[415px] 4K:w-[415px] justify-center rounded-[50%] border-[3px] border-[#76f3ff] bg-[#3A6274] text-[#76f3ff] 4K:border-[10px]";
const className3 =
  "flex flex-col xsm:h-[60px] xsm:w-[60px] sm-xs:h-[70px] sm-xs:w-[70px] sm-s:h-[60px] sm-s:w-[60px] md:h-[100px] md:w-[100px] lg:h-[120px] lg:w-[120px] xl:h-[140px] xl:w-[140px] 2xl:h-[180px] 2xl:w-[180px] 3xl:h-[220px] 3xl:w-[220px] 4K:h-[300px] 4K:w-[300px] justify-center rounded-[50%] border-[3px] border-[#76f3ff] bg-[#3A6274] text-[#76f3ff] 4K:border-[10px]";
const classNameTitel1Formated = "font-bold md:mb-3 4K:mb-10 ";
const classNameTitel2Formated = "font-bold md:mb-1 4K:mb-7 ";

const GameInfo = () => {
  const { round, tosses, money } = useSelector(
    (state: AppState) => state.statTotal
  );
  const { paused } = useSelector((state: AppState) => state.paused);
  const { t } = useTranslation();

  const formatedData = useMemo(() => {
    return {
      formatedNumberRounds: formatNumber(round),
      formatedNumberSnowballTossed: formatNumber(tosses),
      formatedNumberTotalMoney: formatNumber(money),
    };
  }, [round, tosses, money]);

  return (
    <div id="statistikInfo" className="box">
      <div className="w-[100%]">
        <Image
          src="/images/Tabelle.png"
          layout="responsive"
          width={200}
          height={100}
          alt="tabelle"
          draggable="false"
        />
      </div>
      <h1 className="statistic textTitel">{t("statistik.titel1")}</h1>
      <div className="absolute top-[26%] flex h-[64%] w-[80%] text-center ">
        {paused ? (
          <div className="text relative w-[100%]">{t("noround")}</div>
        ) : (
          <>
            <div className="relative w-[100%]">
              <div className="absolute top-[8%] left-[35%]">
                {round > 15 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={round.toLocaleString()}
                      titelFormated={formatedData.formatedNumberRounds}
                      text={t("statistik.text1")}
                      className={classNameRounds}
                      classNameTitelFormated={classNameTitel1Formated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + round / 60,
                    }}
                  >
                    <Circle
                      titel={round.toLocaleString()}
                      titelFormated={formatedData.formatedNumberRounds}
                      text={t("statistik.text1")}
                      className={classNameRounds}
                      classNameTitelFormated={classNameTitel1Formated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute left-[5%] bottom-[10%]">
                {tosses > 605555 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={tosses.toLocaleString()}
                      titelFormated={formatedData.formatedNumberSnowballTossed}
                      text={t("statistik.text2")}
                      className={className3}
                      classNameTitelFormated={classNameTitel2Formated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + tosses / 2422220,
                    }}
                  >
                    <Circle
                      titel={tosses.toLocaleString()}
                      titelFormated={formatedData.formatedNumberSnowballTossed}
                      text={t("statistik.text2")}
                      className={className3}
                      classNameTitelFormated={classNameTitel2Formated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute right-[5%] bottom-[15%]">
                {money > 5000000 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={money.toLocaleString()}
                      titelFormated={formatedData.formatedNumberTotalMoney}
                      text={t("statistik.text3")}
                      className={className3}
                      classNameTitelFormated={classNameTitel2Formated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + money / 20000000,
                    }}
                  >
                    <Circle
                      titel={money.toLocaleString()}
                      titelFormated={formatedData.formatedNumberTotalMoney}
                      text={t("statistik.text3")}
                      className={className3}
                      classNameTitelFormated={classNameTitel2Formated}
                    />
                  </motion.div>
                )}
              </div>
            </div>
            <div className="absolute left-[5%] bottom-[10%]">
              {tosses > 605555 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={tosses.toLocaleString()}
                    titelFormated={formatedData.formatedNumberSnowballTossed}
                    text={t("statistik.text2")}
                    className={className3}
                    classNameTitelFormated={classNameTitel2Formated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + tosses / 2422220,
                  }}
                >
                  <Circle
                    titel={tosses.toLocaleString()}
                    titelFormated={formatedData.formatedNumberSnowballTossed}
                    text={t("statistik.text2")}
                    className={className3}
                    classNameTitelFormated={classNameTitel2Formated}
                  />
                </motion.div>
              )}
            </div>
            <div className="absolute right-[5%] bottom-[15%]">
              {money > 5000000 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={money.toLocaleString()}
                    titelFormated={formatedData.formatedNumberTotalMoney}
                    text={t("statistik.text3")}
                    className={className3}
                    classNameTitelFormated={classNameTitel2Formated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + money / 20000000,
                  }}
                >
                  <Circle
                    titel={money.toLocaleString()}
                    titelFormated={formatedData.formatedNumberTotalMoney}
                    text={t("statistik.text3")}
                    className={className3}
                    classNameTitelFormated={classNameTitel2Formated}
                  />
                </motion.div>
              )}
            </div>
          </>
        )}
      </div>
    </div>
  );
};
export default GameInfo;
