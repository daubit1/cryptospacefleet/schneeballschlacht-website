import nextI18nextConfig from "../../../next-i18next.config";
import languageDetector from "../../lib/languageDetector";
import { useRouter } from "next/router";
import Link from "next/link";
import styles from "./LanguageSwitcher.module.scss";

declare type InternalLanguageSwitchLinkProps = {
  locale: string;
};
declare type LanguageSwitchLinkProps = Omit<
  React.AnchorHTMLAttributes<HTMLAnchorElement>,
  keyof InternalLanguageSwitchLinkProps
> &
  InternalLanguageSwitchLinkProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLAnchorElement>;

const LanguageSwitchLink = ({ locale, ...rest }: LanguageSwitchLinkProps) => {
  const router = useRouter();

  let href = rest.href || router.asPath;
  let pName = router.pathname;
  Object.keys(router.query).forEach((k) => {
    if (k === "locale") {
      pName = pName.replace(`[${k}]`, locale);
      return;
    }
    // @ts-ignore
    pName = pName.replace(`[${k}]`, router.query[k]);
  });
  if (locale) {
    href = rest.href ? `/${locale}${rest.href}` : pName;
  }

  return (
    <Link href={href}>
      <button
        // @ts-ignore
        onClick={() => languageDetector.cache(locale)}
        className={styles.button}
      >
        {locale}
      </button>
    </Link>
  );
};

export default function LanguageSwitcher() {
  return (
    <div className={styles.lngSwitcher}>
      {nextI18nextConfig.i18n.locales.map((locale) => (
        <LanguageSwitchLink locale={locale} key={locale} />
      ))}
    </div>
  );
}
