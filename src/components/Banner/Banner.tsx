import { ParallaxBanner } from "react-scroll-parallax";
import { BannerLayer } from "react-scroll-parallax/dist/components/ParallaxBanner/types";
import { MdLogout } from "react-icons/md";
import Image from "next/image";
import styles from "./Banner.module.scss";

interface BannerProps {
  deactivate: () => void;
  logout: boolean;
}
export default function Banner({ logout, deactivate }: BannerProps) {
  const background: BannerLayer = {
    children: (
      <Image
        width={3840}
        height={2719}
        src="/images/Start_Env_BG.png"
        alt="banner"
        draggable="false"
      />
    ),
    translateY: [0, 10],
    opacity: [1, 0.3],
    scale: [1.05, 1, "easeOutCubic"],
    shouldAlwaysCompleteAnimation: true,
  };

  const headline: BannerLayer = {
    translateY: [0, 30],
    scale: [1, 1.05, "easeOutCubic"],
    shouldAlwaysCompleteAnimation: true,
    expanded: false,
    children: (
      <div className="absolute inset-0 flex flex-col items-center justify-center text-white lg:pb-[20rem]">
        <h1 className="font-[chango] text-3xl lg:text-7xl">SCHNEEBALL</h1>
        <h1 className="font-[chango] text-3xl lg:text-7xl">SCHLACHT</h1>
      </div>
    ),
  };

  const foreground1: BannerLayer = {
    children: (
      <Image
        width={3840}
        height={2719}
        src="/images/Start_Env_FG.png"
        alt="banner"
        draggable="false"
      />
    ),
    translateY: [3, 1],
    scale: [1, 1.1, "easeOutCubic"],
    shouldAlwaysCompleteAnimation: true,
  };
  const foreground0: BannerLayer = {
    children: (
      <Image
        width={3840}
        height={2719}
        src="/images/Start_Env_MG.png"
        alt="banner"
        draggable="false"
      />
    ),
    translateY: [0, 1],
    scale: [1, 1.1, "easeOutCubic"],
    shouldAlwaysCompleteAnimation: true,
  };

  const gradientOverlay: BannerLayer = {
    opacity: [0, 0.9],
    shouldAlwaysCompleteAnimation: true,
    expanded: false,
    children: (
      <div className="absolute inset-0 bg-gradient-to-t from-gray-900 to-blue-900" />
    ),
  };
  return (
    <div className={styles.container}>
      <div className={styles.head}>
        {logout && (
          <MdLogout
            id="logout"
            color="white"
            className={styles.logout}
            onClick={deactivate}
          />
        )}
      </div>
      <div className={styles.logo}>
        <Image
          width={150}
          height={150}
          src="/images/LOGO_256.png"
          alt="logo"
          layout="responsive"
          draggable="false"
        />
      </div>
      <ParallaxBanner
        layers={[
          background,
          foreground0,
          headline,
          foreground1,
          gradientOverlay,
        ]}
        className="h-full overflow-hidden bg-gray-900"
      />
    </div>
  );
}
