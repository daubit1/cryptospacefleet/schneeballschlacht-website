import CookieConsent from "react-cookie-consent";
import { useTranslation } from "react-i18next";
import Analytics from "../../Analytics";

export default function Cookie(): JSX.Element {
  const { t } = useTranslation();
  return (
    <CookieConsent
      enableDeclineButton
      declineButtonText={t("cookie.decline")}
      buttonText={t("cookie.accept")}
      style={{ background: "#3A6274", color: "white" }}
      onAccept={() => Analytics.initialize()}
      onDecline={() => Analytics.removeGACookies()}
      flipButtons
    >
      {t("cookie.description")}
    </CookieConsent>
  );
}
