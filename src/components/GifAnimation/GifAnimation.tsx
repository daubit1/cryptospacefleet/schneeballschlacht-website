import Image from "next/image";
import styles from "./GifAnimation.module.scss";

export default function GifAnimation() {
  return (
    <div className={styles.container}>
      <Image
        width={3840}
        height={2163}
        src="/images/StartseiteAnimGIF.gif"
        alt="animation"
        draggable="false"
      />
    </div>
  );
}
