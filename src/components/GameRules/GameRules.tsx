import { useState } from "react";
import { useTranslation } from "next-i18next";
import Paginate from "react-paginate";
import Link from "next/link";
import Image from "next/image";
import { useRules } from "../../hooks/useRules";

interface RulesInterface {
  selected: number;
}
const GameRules = () => {
  const { t } = useTranslation();
  const Rules = useRules();
  const [pageNumber, setPageNumber] = useState(0);
  const textPerPage = 1;
  const pagesVisited = pageNumber * textPerPage;
  const pageCount = Rules.length;
  const changePage = ({ selected }: RulesInterface) => {
    setPageNumber(selected);
  };
  return (
    <div className="box">
      <div className="w-[100%]">
        <Image
          src="/images/Tabelle.png"
          layout="responsive"
          width={200}
          height={100}
          alt="tabelle"
          draggable="false"
        />
      </div>
      <h1 className="statistic textTitel">{t("gameRules.titel")}</h1>
      <div className="absolute top-[25%] z-[0] w-[100%]">
        <Image
          src="/images/GameRulesBg.jpg"
          width={200}
          height={200}
          layout="responsive"
          alt="rulesBackground"
          draggable="false"
        />
        <div className="absolute top-[45%] left-[6%] w-[30%]">
          <Image
            src="/images/GameRulesExplainer.png"
            width={200}
            height={250}
            layout="responsive"
            alt="explainer"
            draggable="false"
          />
        </div>
        <div className="absolute top-[2%] right-[3.5%] w-[59%] text-center">
          <Image
            src="/images/GameRulesBox.png"
            width={200}
            height={280}
            layout="responsive"
            alt="rulesBox"
            draggable="false"
          />
          {Rules.slice(pagesVisited, pagesVisited + textPerPage).map(
            (rules) => (
              <div
                key={rules.id}
                className="absolute top-[3%] w-[100%] lg:top-[3.5%]"
              >
                <p className="mb-[8%] font-bold md:mb-[13%] text-[#76f3ff] text-[7px] md:text-base">{rules.titel}</p>
                {rules.id === 3 ? (
                  <p className="scrollbar textGame absolute left-[17%] h-[500px] w-[65%] overflow-auto overflow-x-hidden p-[2%] lg:p-[5%] 3xl:h-[1000px]">
                    {rules.text1}
                    <Link href="https://google.de">
                      <a>(Link)</a>
                    </Link>
                    {rules.text2}
                  </p>
                ) : (
                  <p className="scrollbar textGame absolute left-[17%] h-[500px] w-[65%] overflow-auto overflow-x-hidden p-[2%] lg:p-[5%] 3xl:h-[1000px]">
                    {rules.text1}
                  </p>
                )}
              </div>
            )
          )}
          <Paginate
            previousLabel={
              <div className="absolute left-[9%] top-[45%] w-[15%]">
                <Image
                  src="/images/Pfeil_L.png"
                  width={200}
                  height={200}
                  layout="responsive"
                  alt="rulesPfeilLeft"
                  draggable="false"
                />
                <div className="sliderPfeileL absolute left-[22%] top-[19%] z-20 h-[62%] w-[42%] cursor-pointer bg-[#6ab9ce] bg-opacity-0 transition-opacity hover:bg-opacity-40" />
              </div>
            }
            nextLabel={
              <div className="absolute right-[9%] top-[45%] w-[15%]">
                <Image
                  src="/images/Pfeil_R.png"
                  width={200}
                  height={200}
                  layout="responsive"
                  alt="rulesPfeilRight"
                  draggable="false"
                />
                <div
                  className="sliderPfeileR absolute right-[23%] top-[19%] z-20 h-[62%] w-[42%]
    cursor-pointer bg-[#6ab9ce] bg-opacity-0 transition-opacity ease-out hover:bg-opacity-40"
                />
              </div>
            }
            pageCount={pageCount}
            onPageChange={changePage}
            className="text bottom-[30%] left-[45%] space-x-5"
            pageClassName="hidden"
            disabledClassName="hidden"
          />
        </div>
      </div>
    </div>
  );
};

export default GameRules;
