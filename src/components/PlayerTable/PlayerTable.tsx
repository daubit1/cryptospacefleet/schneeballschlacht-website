import { useEffect, useMemo, useState } from "react";
import { BiCaretUp, BiCaretDown } from "react-icons/bi";
import Paginate from "react-paginate";
import styles from "../PlayerTable/PlayerTable.module.scss";
import { StatsBoard } from "../../state/statsLeaderBoardSlice";
import Image from "next/image";

enum Direction {
  Ascending = "ascending",
  Descending = "descending",
}

enum Show {
  Start = "",
  Inline = "inline",
  Hidden = "hidden",
}

interface Config {
  key: keyof StatsBoard;
  direction: Direction;
}

interface Arrow {
  arrow1Level: Show;
  arrow2Level: Show;
  arrow1Balls: Show;
  arrow2Balls: Show;
}

interface sortTableProps {
  state: StatsBoard[];
}

export interface PlayerTabeleProps {
  state: StatsBoard[];
  title: string;
  bg: boolean;
  cursor: string;
  selectedFriend: any;
}

const SortedTableData = ({ state }: sortTableProps, config = null) => {
  const [sortConfig, setSortConfig] = useState<Config | null>(config);
  const [arrow, setArrow] = useState<Arrow>({
    arrow1Level: Show.Start,
    arrow2Level: Show.Start,
    arrow1Balls: Show.Start,
    arrow2Balls: Show.Start,
  });
  const sortedItems = useMemo(() => {
    let sortableItems = [...state];
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (
          a[sortConfig.key as keyof StatsBoard] >
          b[sortConfig.key as keyof StatsBoard]
        ) {
          return sortConfig.direction === Direction.Ascending ? -1 : 1;
        }
        if (
          a[sortConfig.key as keyof StatsBoard] <
          b[sortConfig.key as keyof StatsBoard]
        ) {
          return sortConfig.direction === Direction.Ascending ? 1 : -1;
        }
        return 0;
      });
    }

    return sortableItems;
  }, [sortConfig, state]);

  const sortBy1 = (key: keyof StatsBoard) => {
    let direction = Direction.Ascending;
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === Direction.Ascending
    ) {
      direction = Direction.Descending;
    }
    setSortConfig({ key, direction });
    setArrow({
      arrow1Level: Show.Inline,
      arrow2Level: Show.Hidden,
      arrow1Balls: Show.Hidden,
      arrow2Balls: Show.Hidden,
    });
    if (arrow.arrow1Level === Show.Inline) {
      setArrow({
        arrow1Level: Show.Hidden,
        arrow2Level: Show.Inline,
        arrow1Balls: Show.Hidden,
        arrow2Balls: Show.Hidden,
      });
    }
  };

  const sortBy2 = (key: keyof StatsBoard) => {
    let direction = Direction.Ascending;
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === Direction.Ascending
    ) {
      direction = Direction.Descending;
    }
    setSortConfig({ key, direction });
    setArrow({
      arrow1Level: Show.Hidden,
      arrow2Level: Show.Hidden,
      arrow1Balls: Show.Inline,
      arrow2Balls: Show.Hidden,
    });
    if (arrow.arrow1Balls === Show.Inline) {
      setArrow({
        arrow1Level: Show.Hidden,
        arrow2Level: Show.Hidden,
        arrow1Balls: Show.Hidden,
        arrow2Balls: Show.Inline,
      });
    }
  };

  return {
    items: sortedItems,
    sortBy1,
    sortBy2,
    arrow,
  };
};

const PlayerTable = ({
  state,
  title,
  bg,
  selectedFriend,
  cursor,
}: PlayerTabeleProps) => {
  const { items, sortBy1, sortBy2, arrow } = SortedTableData({ state });
  useEffect(() => {
    sortBy1("levelBoard");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [pageNumber, setPageNumber] = useState(0);
  const tokenPerPage = 10;
  const pagesVisited = pageNumber * tokenPerPage;
  const pageCount = Math.ceil(items?.length / tokenPerPage);
  const changePage = ({ selected }: { selected: number }): void => {
    setPageNumber(selected);
  };
  return (
    <div className={bg ? "box" : "box2"}>
      <div className="w-[100%]">
        <Image
          src="/images/Tabelle.png"
          layout="responsive"
          width={200}
          height={100}
          alt="tabelle"
          draggable="false"
        />
      </div>
      <h1 className="statistic textTitel">{title}</h1>
      <div className="text absolute top-[30%] z-[1] h-[50%] w-[80%]">
        <table className="flex h-[100%] flex-col">
          <thead>
            <tr className="flex 4K:h-[80px]">
              <th className="w-[50%] sm:w-[40%]">Adresse</th>
              <th className="w-[20%] sm:w-[20%]">
                <button onClick={() => sortBy1("levelBoard")}>
                  max lv
                  <BiCaretUp className={arrow.arrow1Level} />
                  <BiCaretDown className={arrow.arrow2Level} />
                </button>
              </th>
              <th className="w-[30%] xsm:w-[40%]">
                <button onClick={() => sortBy2("tossedSnowballBoard")}>
                  ges Bälle gew
                  <BiCaretUp className={arrow.arrow1Balls} />
                  <BiCaretDown className={arrow.arrow2Balls} />
                </button>
              </th>
            </tr>
          </thead>
          <tbody className="scrollbar h-[100%] overflow-y-scroll">
            {items
              .slice(pagesVisited, pagesVisited + tokenPerPage)
              .map((state, index) => {
                return (
                  <tr
                    key={`pl_${index}`}
                    className="align-center flex text-center 4K:h-[80px]"
                  >
                    <td
                      className={`w-[50%] break-words xsm:w-[40%] ${cursor}`}
                      onClick={selectedFriend ? selectedFriend : () => {}}
                    >
                      {state.playerBoard}
                    </td>
                    <td className="w-[20%] xsm:w-[20%]">{state.levelBoard}</td>
                    <td className="w-[30%] xsm:w-[40%]">
                      {state.tossedSnowballBoard}
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <Paginate
          previousLabel={"<"}
          nextLabel={">"}
          pageCount={pageCount}
          onPageChange={changePage}
          containerClassName={styles.paginationButtons}
          previousLinkClassName={styles.previousButton}
          nextLinkClassName={styles.nextButton}
          disabledClassName={styles.disabled}
          activeClassName={styles.aktive}
        />
      </div>
    </div>
  );
};
export default PlayerTable;
