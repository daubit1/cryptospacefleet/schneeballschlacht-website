import { useEffect, useState, useCallback, useMemo } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, EffectCoverflow } from "swiper";
import PlayerTable from "../PlayerTable/PlayerTable";
import { useWeb3React } from "@web3-react/core";
import { BigNumber, ContractReceipt, ethers } from "ethers";
import styles from "./SnowballSelection.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../state";
import { CONTRACT_ABI } from "../../contracts/abi";
import { toast } from "react-toastify";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/effect-coverflow";
import { Spinner } from "../Spinner/Spinner";
import {
  mintSnowball,
  StatsSnowball,
  tossSnowball,
} from "../../state/statsSnowballsSlice";
import "react-toastify/dist/ReactToastify.css";
import { useTranslation } from "next-i18next";
import { Media } from "../../utils/media";
import {
  addToLeaderBoard,
  StatsBoard,
} from "../../state/statsLeaderBoardSlice";
import Image from "next/image";
import { CONTRACT_ADDR, MINT_FEE, TOSS_FEE } from "../../lib/const";
import {
  getLevelupToken,
  getMintedTokenFromLogs,
  hasStone,
  isFinished,
  isPaused,
  isPlayer,
} from "../../lib/getContractData";
import { mintTokenTotal, tossTokenTotal } from "../../state/statsTotalSlice";
import {
  mintTokenPlayerRoundTotal,
  mintTokenRoundTotal,
  tossTokenRoundTotal,
} from "../../state/statsTotalRoundSlice";
import { addToFriendsBoard } from "../../state/statsFriendsBoardSlice";
import useStartRound from "../../hooks/useStartRound";
import useEndRound from "../../hooks/useEndRound";
import useTimeoutCooldownInfo from "../../hooks/useTimeoutCooldownInfo";

interface SnowballSelectionProps {
  state: StatsBoard[];
  title: string;
  bg: boolean;
  account: string | undefined;
  chainId: number;
}
const SnowballSelection = ({
  state,
  title,
  bg,
  account,
  chainId,
}: SnowballSelectionProps) => {
  const { friends } = useSelector((state: AppState) => state.statsFriends);
  const { snowballs } = useSelector((state: AppState) => state.statsSnowballs);
  const { player } = useSelector((state: AppState) => state.statsLeader);
  const { paused } = useSelector((state: AppState) => state.paused);
  const { finished } = useSelector((state: AppState) => state.finished);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [addressPlayer, setAddressPlayer] = useState("");
  const snowbawTossed = useMemo(
    () =>
      player.filter(function (e) {
        return e.playerBoard == account;
      }),
    [account, player]
  );
  const [timeout, cooldown] = useTimeoutCooldownInfo(chainId);
  const [minLevel, setMinLevel] = useState(
    snowballs.length ? snowballs[0].partnerCount : 0
  );
  const [level, setLevel] = useState(
    snowballs.length ? snowballs[0].levelSnowball : 0
  );
  const [snowball, setSnowball] = useState(snowballs[0]);
  const [currSnowballId, setCurrSnowballId] = useState(snowball?.snowball);
  const [imgSrc, setImgSrc] = useState<undefined | string>("");

  useEffect(() => {
    setImgSrc(
      snowballs.length
        ? `/images/LVL${
            snowballs[0].levelSnowball >= 10
              ? snowballs[0].levelSnowball
              : "0" + snowballs[0].levelSnowball
          }.png`
        : ""
    );
    setLevel(snowballs.length ? snowballs[0].levelSnowball : 0);
    setMinLevel(snowballs.length ? snowballs[0].partnerCount : 0);
    setSnowball(snowballs[0]);

    const snowball = snowballs.find((x) => x.snowball === currSnowballId);
    if (snowball) {
      const schneeballObj: StatsSnowball = {
        snowball: snowball.snowball,
        levelSnowball: snowball.levelSnowball,
        partnerCount: snowball.partnerCount,
      };
      setSnowball(schneeballObj);
      setLevel(schneeballObj.levelSnowball);
      setMinLevel(schneeballObj.partnerCount);
      setImgSrc(
        `/images/LVL${
          schneeballObj.levelSnowball >= 10
            ? schneeballObj.levelSnowball
            : "0" + schneeballObj.levelSnowball
        }.png`
      );
    }
  }, [currSnowballId, snowballs, snowbawTossed]);
  const selectedFriend = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAddressPlayer(e.target.innerText);
  };
  const handleAddressPlayer = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAddressPlayer(e.target.value);
  };

  const context = useWeb3React();
  const getContract = useCallback(() => {
    try {
      const provider = context.provider!;
      const signer = provider.getSigner();
      const contract = new ethers.Contract(
        CONTRACT_ADDR,
        CONTRACT_ABI,
        provider
      );
      return contract.connect(signer);
    } catch (e) {
      throw e;
    }
  }, [context.provider]);

  const toss = useCallback(async () => {
    if (snowball && addressPlayer.length > 0) {
      try {
        const contract = getContract();
        const tosstsx = await contract["toss(address,uint256)"](
          addressPlayer,
          snowball.snowball,
          {
            value: TOSS_FEE.mul(snowball.levelSnowball),
          }
        );
        console.log("tosstsx", tosstsx);
        const receipt: ContractReceipt = await tosstsx.wait();
        console.log(receipt);
        const tokenIds = getMintedTokenFromLogs(receipt);
        const stone = hasStone(receipt);
        const levelupToken = getLevelupToken(receipt);
        dispatch(tossTokenTotal());
        dispatch(
          tossTokenRoundTotal({
            level: snowball.levelSnowball,
            hasStone: stone,
          })
        );
        dispatch(tossSnowball(snowball.snowball));
        if (await isPlayer(contract, addressPlayer)) {
          dispatch(mintTokenPlayerRoundTotal());
        }
        dispatch(
          addToLeaderBoard({
            addr: addressPlayer,
            level: snowball.levelSnowball,
          })
        );
        dispatch(
          addToFriendsBoard({
            addr: addressPlayer,
            level: snowball.levelSnowball,
          })
        );
        if (levelupToken) {
          dispatch(mintTokenTotal());
          const levelupTokenTo = tokenIds.find(
            (x) => x.id === parseInt(levelupToken.tokenId)
          )?.to;
          if (levelupTokenTo) {
            dispatch(
              addToLeaderBoard({
                addr: levelupTokenTo,
                level: snowball.levelSnowball + 1,
              })
            );
          }
          toast(
            `${levelupTokenTo} got a level ${
              snowball.levelSnowball + 1
            } Snowball`
          );
          // TODO: trim leading 0s from addresses
          if (account && levelupTokenTo === account) {
            dispatch(
              mintSnowball({
                id: parseInt(levelupToken.tokenId),
                level: snowball.levelSnowball + 1,
              })
            );
          }
        }
        // TODO: update friendslist + leaderboard, snowball toss count
        toast.success(t("snowballselection.tossed"));
        // levelup: 0x3b7ca85b5e3d1ebb3a81e8d9e27bfeb9ef2f447026c115421132a20b0733b73f
        // toss: 0xd23251b81754317af62d993d1b310e9defa61d871b8aee950aa0e2bb26ff5154
        const cooldownResult = await cooldown(account!);
        if (cooldownResult[0]) {
          const msg = `${t("snowballselection.cooldownnowtill")} ${
            cooldownResult[1]
          }`;
          toast.success(msg, {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            draggable: true,
            progress: undefined,
          });
        }
      } catch (e) {
        console.log(e);
        if ((e as Error).message.includes("Cooldown")) {
          const cooldownResult = await cooldown(account!);
          if (cooldownResult[0]) {
            const msg = `${t("snowballselection.cooldowntill")} ${
              cooldownResult[1]
            }`;
            toast.error(msg, {
              position: "bottom-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            const msg = t("snowballselection.txfail");
            toast.error(msg, {
              position: "bottom-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              draggable: true,
              progress: undefined,
            });
          }
        } else if ((e as Error).message.includes("Timeout")) {
          const timeoutResult = await timeout(account!);
          if (timeoutResult[0]) {
            const msg = `${t("snowballselection.timeouttill")} ${
              timeoutResult[1]
            }`;
            toast.error(msg, {
              position: "bottom-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            const msg = t("snowballselection.txfail");
            toast.error(msg, {
              position: "bottom-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              draggable: true,
              progress: undefined,
            });
          }
        } else {
          const msg = t("snowballselection.txfail");
          toast.error(msg, {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            draggable: true,
            progress: undefined,
          });
        }
      }
    }
    if (addressPlayer.length === 0) {
      toast(t("snowballselection.enteraddr"));
    }
  }, [
    account,
    addressPlayer,
    cooldown,
    dispatch,
    getContract,
    snowball,
    t,
    timeout,
  ]);

  const startRound = useStartRound(context.account!);
  const stopRound = useEndRound();

  const payout = useCallback(async () => {
    if (finished && !paused) {
      try {
        const contract = getContract();
        const round = (await contract["getRoundId()"]()).toNumber();
        for (let i = 1; i <= round; i++) {
          const deposits: BigNumber = await contract[
            "depositsOf(uint256,address)"
          ](i, account);
          if (deposits.toNumber() > 0) {
            const withdraw = await contract["withdraw(uint256,address)"](
              i,
              account
            );
            console.log("withdraw", withdraw);
            const waitWithdraw = withdraw.wait();
            toast.promise(waitWithdraw, {
              pending: t("snowballselection.txpending") as string,
              success: t("snowballselection.withdraw") as string,
              error: t("snowballselection.txfail") as string,
            });
          }
        }
      } catch (e) {
        let msg = t("snowballselection.txfail");
        toast.error(msg, {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          draggable: true,
          progress: undefined,
        });
        console.log(e);
      }
    }
  }, [account, finished, getContract, paused, t]);

  const mint = useCallback(async () => {
    try {
      const contract = getContract();
      const minttsx = await contract["mint(address)"](account, {
        value: MINT_FEE,
      });
      console.log("minttsx", minttsx);
      const wait: ContractReceipt = await minttsx.wait();
      const token = getMintedTokenFromLogs(wait);
      dispatch(mintTokenTotal());
      dispatch(mintTokenRoundTotal());
      if (snowballs.length === 0) {
        dispatch(mintTokenPlayerRoundTotal());
      }
      dispatch(mintSnowball({ id: token[0].id, level: 1 }));
      toast.success(t("snowballselection.minted"));
    } catch (e) {
      let msg = t("snowballselection.txfail");
      toast.error(msg, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        draggable: true,
        progress: undefined,
      });
      console.log(e);
    }
  }, [account, dispatch, getContract, snowballs.length, t]);

  return (
    <div>
      <div className="box">
        <div className="slider container">
          <span className="text absolute top-[1%] z-10 w-[100%] text-center font-bold xsm:text-xl sm-xs:text-3xl md:text-4xl xl:text-7xl 4K:text-9xl">
            Game
          </span>
          <div className="w-[100%]">
            <Image
              src="/images/Game_BG.jpg"
              layout="responsive"
              width={100}
              height={200}
              alt="Game background"
              draggable="false"
            />
            <div className="absolute top-[7.5%] left-[-1%] z-10 w-[40%]">
              <Image
                src="/images/Box_UR.png"
                layout="responsive"
                width={120}
                height={70}
                alt="topR"
                draggable="false"
              />
            </div>
            <div className="absolute top-[7.5%] right-[-1%] z-10 w-[40%]">
              <Image
                src="/images/Box_UL.png"
                layout="responsive"
                width={406}
                height={230}
                alt="topL"
                draggable="false"
              />
            </div>
            <div className="absolute top-[55%] left-[5%] w-[40%]">
              <div>
                <Image
                  src="/images/Box_OR.png"
                  layout="responsive"
                  width={406}
                  height={230}
                  alt="bottomL"
                  draggable="false"
                />
              </div>
              {!finished && paused ? (
                <button
                  className="buttonLClipPatch absolute top-[19%] left-[13%] z-20 flex h-[52%] w-[71%] cursor-pointer items-center justify-center border-none bg-[#6ab9ce] bg-opacity-0 p-0 transition-opacity hover:bg-opacity-40"
                  onClick={startRound}
                >
                  <span className="sm-xs:text-xsm font-bold text-[#76f3ff] sm-s:text-base md:text-xl lg:text-2xl 2xl:text-3xl 4K:text-5xl">
                    Start
                  </span>
                </button>
              ) : (
                <button
                  className="buttonLClipPatch absolute top-[21%] left-[13%] z-20 flex h-[50%] w-[71%] cursor-pointer items-center justify-center border-none bg-[#6ab9ce] bg-opacity-0 p-0 transition-opacity hover:bg-opacity-40"
                  onClick={stopRound}
                  aria-disabled={!(finished && paused)}
                >
                  <span className="sm-xs:text-xsm font-bold text-[#76f3ff] sm-s:text-base md:text-xl lg:text-2xl 2xl:text-3xl 4K:text-5xl">
                    Stop
                  </span>
                </button>
              )}
            </div>
            <div className="absolute top-[61.9%] left-[6%] w-[31%]">
              <div>
                <Image
                  src="/images/Box_OR.png"
                  layout="responsive"
                  width={406}
                  height={230}
                  alt="bottomL"
                  draggable="false"
                />
              </div>
              <button
                className="buttonLClipPatch absolute top-[20.5%] left-[13%] z-20 flex h-[50.5%] w-[70.7%] cursor-pointer items-center justify-center border-none bg-[#6ab9ce] bg-opacity-0 p-0 transition-opacity hover:bg-opacity-40"
                onClick={payout}
                aria-disabled={!(finished && !paused)}
              >
                <span className="sm-xs:text-xsm font-bold text-[#76f3ff] xsm:text-[10px] sm-s:text-sm md:text-xl lg:text-2xl 2xl:text-3xl 4K:text-5xl">
                  {t("snowballselection.payout")}
                </span>
              </button>
            </div>
            <div className="absolute top-[61.9%] right-[6%] w-[31%]">
              <div>
                <Image
                  src="/images/Box_OL.png"
                  layout="responsive"
                  width={406}
                  height={230}
                  alt="bottomR"
                  draggable="false"
                />
              </div>
              <button
                className="buttonRClipPatch absolute top-[20.4%] left-[17%] z-20 flex h-[50.1%] w-[71%] cursor-pointer items-center justify-center border-none bg-[#6ab9ce] bg-opacity-0 p-0 transition-opacity hover:bg-opacity-40"
                onClick={mint}
              >
                <span className="sm-xs:text-xsm font-bold text-[#76f3ff] xsm:text-[10px] sm-s:text-sm md:text-xl lg:text-2xl 2xl:text-3xl 4K:text-5xl">
                  Mint
                </span>
              </button>
            </div>
            <div className="absolute top-[55%] right-[5%] z-10 w-[40%]">
              <div>
                <Image
                  src="/images/Box_OL.png"
                  layout="responsive"
                  width={406}
                  height={230}
                  alt="bottomR"
                  draggable="false"
                />
              </div>
              <button
                className="buttonRClipPatch absolute top-[19.8%] left-[16.8%] flex h-[51.2%] w-[71%] cursor-pointer items-center justify-center border-none bg-[#6ab9ce] bg-opacity-0 p-0 transition-opacity hover:bg-opacity-40"
                onClick={toss}
                aria-disabled={!(snowballs.length && addressPlayer)}
              >
                <span className="xsm:text-md font-bold text-[#76f3ff] sm-xs:text-xl sm:text-xl md:text-2xl lg:text-3xl 2xl:text-4xl 4K:text-5xl">
                  {t("snowballselection.toss")}
                </span>
              </button>
            </div>
            <div className="absolute top-[54%] right-[26.8%] w-[46%]">
              <Image
                src="/images/Box_LvL.png"
                layout="responsive"
                width={100}
                height={200}
                alt="levelBox"
                draggable="false"
              />
            </div>
            {imgSrc ? (
              <div className="absolute top-[18%] left-[21%] w-[57%] text-center">
                <Image
                  src={imgSrc}
                  width={500}
                  height={500}
                  layout="responsive"
                  alt="snowball"
                  draggable="false"
                />
              </div>
            ) : (
              <div
                className={`absolute top-[35%] left-[50%] z-10 ${styles.spinner}`}
              >
                <Spinner />
              </div>
            )}
            <div className="sl">
              <Swiper
                className="h-400 top-[-41%]"
                effect={"coverflow"}
                modules={[Navigation, EffectCoverflow]}
                autoHeight={true}
                grabCursor={true}
                spaceBetween={10}
                slidesPerView={3.8}
                height={400}
                coverflowEffect={{
                  rotate: 0,
                  stretch: 0,
                  depth: 180,
                  modifier: 1,
                  slideShadows: false,
                }}
                navigation={{
                  nextEl: "#swiper-forward",
                  prevEl: "#swiper-back",
                }}
                centeredSlides={true}
                onSlideChange={(e: any) => {
                  const schneeball = e.slides[e.activeIndex];
                  setCurrSnowballId(+schneeball.dataset.id);
                }}
              >
                {snowballs.map((snowball: StatsSnowball, index: number) => (
                  <SwiperSlide
                    key={`sl_${index}`}
                    className="h-60"
                    data-id={snowball.snowball}
                    data-level={snowball.levelSnowball}
                    data-partner={snowball.partnerCount}
                  >
                    <Media greaterThanOrEqual="xxl">
                      <Image
                        src={`/images/LVL${
                          snowball.levelSnowball >= 10
                            ? snowball.levelSnowball
                            : "0" + snowball.levelSnowball
                        }.png`}
                        width={800}
                        height={800}
                        alt="snowball"
                        draggable="false"
                      />
                    </Media>
                    <Media lessThan="xxl">
                      <Image
                        src={`/images/LVL${
                          snowball.levelSnowball >= 10
                            ? snowball.levelSnowball
                            : "0" + snowball.levelSnowball
                        }.png`}
                        width={400}
                        height={400}
                        alt="snowball"
                        draggable="false"
                      />
                    </Media>
                  </SwiperSlide>
                ))}
              </Swiper>
            </div>
            <div className="absolute left-[4%] top-[66%] z-10 w-[36%]">
              <Image
                src="/images/Pfeil_L.png"
                width={219}
                height={275}
                layout="responsive"
                alt="rulesPfeilLeft"
                draggable="false"
              />
              <div
                id="swiper-back"
                className="sliderPfeileL absolute left-[22%] top-[19%] z-50 h-[62%] w-[42%] cursor-pointer bg-[#6ab9ce] bg-opacity-0 transition-opacity hover:bg-opacity-40"
              />
            </div>
            <div className="absolute right-[2%] top-[66%] z-10 w-[36%]">
              <Image
                src="/images/Pfeil_R.png"
                width={219}
                height={275}
                layout="responsive"
                alt="rulesPfeilRight"
                draggable="false"
              />
              <div
                id="swiper-forward"
                className="sliderPfeileR absolute right-[23%] top-[19%] z-50 h-[62%] w-[42%]
    cursor-pointer bg-[#6ab9ce] bg-opacity-0 transition-opacity ease-out hover:bg-opacity-40"
              />
            </div>
          </div>
        </div>
        <div className="absolute top-[47%] w-[100%]">
          <Image
            src="/images/Eingabefeld_Adresse2.png"
            width={2000}
            height={406}
            layout="responsive"
            alt="adresse"
            draggable="false"
          />
        </div>
        <div className={styles.progress}>
          <Image
            src="/images/Box_LvL.png"
            layout="responsive"
            width={100}
            height={200}
            alt="levelBox"
            draggable="false"
          />
          <div className={styles.level}>
            <span className={styles.number}>{level}</span>
          </div>
          <div className={styles.loadBar}>
            <div
              className={styles.bar}
              style={{ height: Number(`${level}`) * 5 + "%" }}
            ></div>
            <div className={styles.levelMinMax}>
              {minLevel}/{level == 20 ? "20" : Number(`${level}`) + 1}
            </div>
          </div>
        </div>
        <input
          className={`text absolute top-[20.5%] h-5 w-[80%] rounded-sm bg-[rgba(58,98,116,0.7)] py-5 text-center text-3xl font-bold lg:top-[50.5%] lg:h-10 ${styles.input}`}
          value={addressPlayer}
          onChange={handleAddressPlayer}
        />
        {!addressPlayer && (
          <div
            className={`sm-xs:text-xsm absolute top-[53.1%] w-[100%] bg-transparent py-5 text-center text-[#76f3ff] xsm:text-[10px] sm-s:text-sm md:text-xl lg:text-2xl 2xl:text-3xl 4K:text-5xl ${styles.warning}`}
          >
            {t("snowballselection.warning")}
          </div>
        )}
      </div>
      <PlayerTable
        state={friends}
        title={t("statistik.titel4")}
        bg={false}
        cursor="cursor-pointer"
        selectedFriend={selectedFriend}
      />
    </div>
  );
};

export default SnowballSelection;
