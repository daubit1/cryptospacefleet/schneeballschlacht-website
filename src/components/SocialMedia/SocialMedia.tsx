import { useTranslation } from "next-i18next";
import Image from "next/image";

const SocialMedia = () => {
  const { t } = useTranslation();
  return (
    <div className="mb-[2rem] flex flex-col items-center bg-white pt-[8rem] pb-1 sm-s:pt-24 sm:pb-2 lg:pt-[10rem] lg:pb-3 4K:pt-[15rem]">
      <div className="flex flex-row justify-center gap-2">
        {/* <a className="mr-2 md:mr-5" href="https://google.de">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px]">
            <Image
              src="/images/TikTok.svg"
              alt="tiktok"
              layout="responsive"
              width={100}
              height={100}
            />
          </div>
        </a> */}
        {/* <a className="mr-2 md:mr-5" href="https://google.de">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px]">
            <Image
              src="/images/Instagram.png"
              alt="instagram"
              layout="responsive"
              width={100}
              height={100}
            />
          </div>
        </a> */}
        {/* <a className="mr-2 md:mr-5" href="https://google.de">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px]">
            <Image
              src="/images/Telegram.svg"
              alt="telegram"
              layout="responsive"
              width={100}
              height={100}
            />
          </div>
        </a> */}
        <a href="https://twitter.com/DaubitGmbH">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px]">
            <Image
              src="/images/Twitter.svg"
              alt="twitter"
              layout="responsive"
              width={100}
              height={100}
              draggable="false"
            />
          </div>
        </a>
        <a href="https://discord.com/invite/ufk79MaAYu?utm_source=WEBSITE&utm_medium=WEBSITE&utm_campaign=WEBSITE">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px]">
            <Image
              src="/images/Discord.svg"
              alt="discord"
              layout="responsive"
              width={100}
              height={100}
              draggable="false"
            />
          </div>
        </a>
        <a href="https://daubitprojectcore.com/">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px] ">
            <Image
              src="/images/LOGO_Projectcore.png"
              alt="projectcore_logo"
              layout="responsive"
              width={100}
              height={100}
              draggable="false"
            />
          </div>
        </a>
        <a href="https://daubit.org/">
          <div className="w-7 md:w-9 lg:w-14 4K:w-[100px]">
            <Image
              src="/images/Logo.jpg"
              alt="logo_daubit"
              layout="responsive"
              width={100}
              height={100}
              draggable="false"
            />
          </div>
        </a>
      </div>
      <a href="https://daubit.org/impressum">{t("imprint")}</a>
    </div>
  );
};
export default SocialMedia;
