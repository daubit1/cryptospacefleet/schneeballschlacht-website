import { toast } from "react-toastify";
import { useCallback } from "react";
import { useWeb3React } from "@web3-react/core";
import { useTranslation } from "next-i18next";
import { Network } from "../lib/network";
import { getNetwork } from "../lib/functions";

interface Props {
  network: Network;
}

export const AddNetwork = ({ network }: Props) => {
  const context = useWeb3React();
  const { t } = useTranslation();

  const addNetwork = useCallback(async () => {
    const params = getNetwork(network);
    if (
      window.ethereum &&
      // @ts-ignore
      window.ethereum.providers &&
      // @ts-ignore
      window.ethereum.providers.length > 1
    ) {
      // @ts-ignore
      const network = await window.ethereum.providers[1].request({
        method: "net_version",
      });
      if (Number(network) === Number(params.chainId)) {
        toast.info(t("connected"));
        return;
      }
      // @ts-ignore
      window.ethereum.providers[1].request({
        method: "wallet_addEthereumChain",
        params: [params, context.account],
      });
    } else if (
      (window.ethereum &&
        // @ts-ignore
        window.ethereum.providers) ||
      window.ethereum
    ) {
      // @ts-ignore
      const network = await window.ethereum.request({
        method: "net_version",
      });
      if (Number(network) === Number(params.chainId)) {
        toast.info(t("connected"));
        return;
      }
      // @ts-ignore
      await window.ethereum.request({
        method: "wallet_addEthereumChain",
        params: [params, context.account],
      });
    }
  }, [context.account, network, t]);
  return (
    <button className="round-btn" onClick={addNetwork}>
      {t("addNetwork")}
    </button>
  );
};
