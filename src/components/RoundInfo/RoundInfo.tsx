import { useTranslation } from "next-i18next";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import Circle from "../Circle/Circle";
import { motion } from "framer-motion";
import { formatNumber } from "../../lib/functions";
import { AppState } from "../../state";
import Image from "next/image";

const className1 =
  "flex flex-col xsm:h-[75px] xsm:w-[75px] sm-xs:h-[85px] sm-xs:w-[85px] sm-s:h-[95px] sm-s:w-[95px] md:h-[115px] md:w-[115px] lg:h-[145px] lg:w-[145px] xl:h-[165px] xl:w-[165px] 2xl:h-[215px] 2xl:w-[215px] 3xl:h-[265px] 3xl:w-[265px] 4K:h-[415px] 4K:w-[415px] justify-center rounded-[50%] border-[3px] border-[#76f3ff] bg-[#76f3ff]/[0.4] text-[#76f3ff] 4K:border-[10px]";
const className2 =
  "flex flex-col xsm:h-[60px] xsm:w-[60px] sm-xs:h-[50px] sm-xs:w-[50px] sm-s:h-[70px] sm-s:w-[70px] md:h-[80px] md:w-[80px] lg:h-[100px] lg:w-[100px] xl:h-[120px] xl:w-[120px] 2xl:h-[150px] 2xl:w-[150px] 3xl:h-[200px] 3xl:w-[200px] 4K:h-[300px] 4K:w-[300px] justify-center rounded-[50%] border-[3px] border-[#76f3ff] bg-[#76f3ff]/[0.4] text-[#76f3ff] 4K:border-[10px]";
const classNameTitelFormated = "font-bold md:mb-1 4K:mb-10 4K:text-5xl";

const RoundInfo = () => {
  const { currentHighestLevel, players, stones, tosses, money, snowballs } =
    useSelector((state: AppState) => state.statsTotalRound);
  const { paused } = useSelector((state: AppState) => state.paused);
  const { t } = useTranslation();

  const formatedData = useMemo(() => {
    return {
      formatedNumberLevel: formatNumber(currentHighestLevel),
      formatedNumberSnowballTossed: formatNumber(tosses),
      formatedNumberMoney: formatNumber(money),
      formatedNumberSnowball: formatNumber(snowballs),
      formatedNumberPlayer: formatNumber(players),
      formatedNumberStones: formatNumber(stones),
    };
  }, [currentHighestLevel, tosses, money, snowballs, players, stones]);

  return (
    <div className="box">
      <div className="w-[100%]">
        <Image
          src="/images/Tafel.png"
          layout="responsive"
          width={200}
          height={200}
          alt="tafel"
          draggable="false"
        />
      </div>
      <h1 className="textTitel absolute top-[4.2%] sm-xs:top-[4%] sm-s:top-[4.8%] sm:top-[3.5%] md:top-[4.5%] lg:top-[5%] 2xl:top-[5.5%] 4K:top-[6%]">
        {t("statistik.titel2")}
      </h1>
      <div className="absolute top-[14%] flex h-[70%] w-[80%] text-center ">
        {paused ? (
          <div className="text relative w-[100%]">{t("noround")}</div>
        ) : (
          <>
            <div className="relative w-[100%]">
              <div className="absolute left-[10%] bottom-[5%]">
                {snowballs > 5000 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={snowballs.toLocaleString()}
                      titelFormated={formatedData.formatedNumberSnowball}
                      text={t("statistik.text7")}
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + snowballs / 20000,
                    }}
                  >
                    <Circle
                      titel={snowballs.toLocaleString()}
                      titelFormated={formatedData.formatedNumberSnowball}
                      text={t("statistik.text7")}
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute left-[5%] top-[5%]">
                {Number(currentHighestLevel) > 20 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={Number(currentHighestLevel).toLocaleString()}
                      titelFormated={formatedData.formatedNumberLevel}
                      text="Level"
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + Number(currentHighestLevel) / 60,
                    }}
                  >
                    <Circle
                      titel={Number(currentHighestLevel).toLocaleString()}
                      titelFormated={formatedData.formatedNumberLevel}
                      text="Level"
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute right-[5%] top-[10%]">
                {players > 1000 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={players.toLocaleString()}
                      titelFormated={formatedData.formatedNumberPlayer}
                      text={t("statistik.text4")}
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + players / 4000,
                    }}
                  >
                    <Circle
                      titel={players.toLocaleString()}
                      titelFormated={formatedData.formatedNumberPlayer}
                      text={t("statistik.text4")}
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute right-0 top-[50%]">
                {stones > 1000 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={stones.toLocaleString()}
                      titelFormated={formatedData.formatedNumberStones}
                      text={t("statistik.text5")}
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + stones / 4000,
                    }}
                  >
                    <Circle
                      titel={stones.toLocaleString()}
                      titelFormated={formatedData.formatedNumberStones}
                      text={t("statistik.text5")}
                      className={className2}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute left-[25%] bottom-[40%]">
                {tosses > 605555 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={tosses.toLocaleString()}
                      titelFormated={formatedData.formatedNumberSnowballTossed}
                      text={t("statistik.text2")}
                      className={className1}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + tosses / 2422220,
                    }}
                  >
                    <Circle
                      titel={tosses.toLocaleString()}
                      titelFormated={formatedData.formatedNumberSnowballTossed}
                      text={t("statistik.text2")}
                      className={className1}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                )}
              </div>
              <div className="absolute right-[25%] bottom-[10%]">
                {money > 5000000 ? (
                  <motion.div
                    animate={{
                      scale: 1.25,
                    }}
                  >
                    <Circle
                      titel={money.toLocaleString()}
                      titelFormated={formatedData.formatedNumberMoney}
                      text={t("statistik.text6")}
                      className={className1}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    animate={{
                      scale: 1 + money / 20000000,
                    }}
                  >
                    <Circle
                      titel={money.toLocaleString()}
                      titelFormated={formatedData.formatedNumberMoney}
                      text={t("statistik.text6")}
                      className={className1}
                      classNameTitelFormated={classNameTitelFormated}
                    />
                  </motion.div>
                )}
              </div>
            </div>
            <div className="absolute left-[5%] top-[5%]">
              {Number(currentHighestLevel) > 20 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={Number(currentHighestLevel).toLocaleString()}
                    titelFormated={formatedData.formatedNumberLevel}
                    text="Level"
                    className={className2}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + Number(currentHighestLevel) / 60,
                  }}
                >
                  <Circle
                    titel={Number(currentHighestLevel).toLocaleString()}
                    titelFormated={formatedData.formatedNumberLevel}
                    text="Level"
                    className={className2}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              )}
            </div>
            <div className="absolute right-[5%] top-[10%]">
              {players > 1000 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={players.toLocaleString()}
                    titelFormated={formatedData.formatedNumberPlayer}
                    text={t("statistik.text4")}
                    className={className2}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + players / 4000,
                  }}
                >
                  <Circle
                    titel={players.toLocaleString()}
                    titelFormated={formatedData.formatedNumberPlayer}
                    text={t("statistik.text4")}
                    className={className2}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              )}
            </div>
            <div className="absolute right-0 top-[50%]">
              {stones > 1000 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={stones.toLocaleString()}
                    titelFormated={formatedData.formatedNumberStones}
                    text={t("statistik.text5")}
                    className={className2}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + stones / 4000,
                  }}
                >
                  <Circle
                    titel={stones.toLocaleString()}
                    titelFormated={formatedData.formatedNumberStones}
                    text={t("statistik.text5")}
                    className={className2}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              )}
            </div>
            <div className="absolute left-[25%] bottom-[40%]">
              {tosses > 605555 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={tosses.toLocaleString()}
                    titelFormated={formatedData.formatedNumberSnowballTossed}
                    text={t("statistik.text2")}
                    className={className1}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + tosses / 2422220,
                  }}
                >
                  <Circle
                    titel={tosses.toLocaleString()}
                    titelFormated={formatedData.formatedNumberSnowballTossed}
                    text={t("statistik.text2")}
                    className={className1}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              )}
            </div>
            <div className="absolute right-[25%] bottom-[10%]">
              {money > 5000000 ? (
                <motion.div
                  animate={{
                    scale: 1.25,
                  }}
                >
                  <Circle
                    titel={money.toLocaleString()}
                    titelFormated={formatedData.formatedNumberMoney}
                    text={t("statistik.text6")}
                    className={className1}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              ) : (
                <motion.div
                  animate={{
                    scale: 1 + money / 20000000,
                  }}
                >
                  <Circle
                    titel={money.toLocaleString()}
                    titelFormated={formatedData.formatedNumberMoney}
                    text={t("statistik.text6")}
                    className={className1}
                    classNameTitelFormated={classNameTitelFormated}
                  />
                </motion.div>
              )}
            </div>
          </>
        )}
      </div>
    </div>
  );
};
export default RoundInfo;
