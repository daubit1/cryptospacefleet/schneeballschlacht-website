import { CSSProperties } from "react";
import { BeatLoader } from "react-spinners";
const style: CSSProperties = {
  display: "flex",
  alignItems: "center",
  marginTop: "400px",
};
export const Spinner = () => {
  return (
    <div className="align-center flex justify-center">
      <BeatLoader size={50} cssOverride={style} />
    </div>
  );
};
