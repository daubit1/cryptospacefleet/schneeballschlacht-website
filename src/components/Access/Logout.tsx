import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";

interface LogoutProps {
  deactivate: () => void;
}

export default function Logout({ deactivate }: LogoutProps) {
  return (
    <>
      <Menu
        as="div"
        className="absolute inline-block text-left drop-shadow-lg"
      >
        <div>
          <Menu.Button
            className="block h-24 w-24 justify-center rounded-full border-4 border-white bg-[rgb(58,98,116)] px-4 py-2 text-sm font-medium text-white shadow-sm  hover:bg-[rgb(46,78,94)] focus:outline-none focus:ring-2  focus:ring-offset-2 focus:ring-offset-gray-100 lg:h-36 lg:w-36"
            onClick={deactivate}
          >
            Logout
          </Menu.Button>
        </div>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 mt-2 w-36 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div className="py-1"></div>
          </Menu.Items>
        </Transition>
      </Menu>
    </>
  );
}
