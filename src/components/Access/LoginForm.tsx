import { useWeb3React } from "@web3-react/core";
import Login from "./Login";
import Logout from "./Logout";
import { Network } from "../../lib/network";
import { useEffect } from "react";
import useRefreshData from "../../hooks/useRefreshData";
import { AddNetwork } from "../AddNetwork";

interface Props {
  network: Network;
  deactivate: () => void;
  round: number | "latest";
}

export default function LoginForm({ network, deactivate, round }: Props) {
  const context = useWeb3React();
  const LogIn = useRefreshData(context.account!, round);

  useEffect(() => {
    LogIn();
  }, [LogIn]);

  return (
    <div className="flex w-full flex-row justify-center">
      {!context.isActive || context.chainId !== network ? (
        <Login network={network} />
      ) : (
        <Logout deactivate={deactivate} />
      )}
    </div>
  );
}
