import { Fragment, useCallback } from "react";
import { Menu, Transition } from "@headlessui/react";
import { metaMask } from "../connectors/metamask";
import { coinbaseWallet } from "../connectors/coinbase";
import { useRouter } from "next/router";
import { LoginProps } from "../../lib/network";
import { Media } from "../../utils/media";
import Image from "next/image";
import { AddNetwork } from "../AddNetwork";

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

export default function Login({ network }: LoginProps) {
  const router = useRouter();

  const logInMetamask = useCallback(async () => {
    try {
      await metaMask.activate(network);
      router.push("#statistikInfo");
    } catch (e) {
      console.log(e);
      await metaMask.resetState();
    }
  }, [network, router]);

  const logInCoinbase = useCallback(async () => {
    try {
      await coinbaseWallet.activate(network);
      router.push("#statistikInfo");
    } catch (e) {
      console.log(e);
      coinbaseWallet.deactivate();
    }
  }, [network, router]);

  return (
    <Menu
      as="div"
      className="absolute inline-block text-left drop-shadow-lg xsm:my-[2px] sm-s:mt-[14px] lg:mt-[-5px] xl-l:mt-[-24px] 4K:mt-[-93px]"
    >
      <div className="flex flex-row gap-3">
        <Menu.Button className="round-btn">
          Login
        </Menu.Button>
        <AddNetwork network={network} />
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="mt-[-230px] w-36 origin-top-right cursor-pointer rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none 4K:w-72">
          <div className="py-1">
            <Menu.Item>
              {({ active }: any) => (
                <a
                  className={classNames(
                    active ? "bg-gray-100 text-gray-900" : "text-gray-700",
                    "block px-4 py-2 text-sm 4K:text-3xl"
                  )}
                >
                  <Media greaterThanOrEqual="xxl">
                    <div
                      id="metamask"
                      className="flex justify-between "
                      onClick={logInMetamask}
                    >
                      <Image
                        src={"/images/metamask.png"}
                        width={37}
                        height={25}
                        alt="metamask-icon"
                        draggable="false"
                      />
                      Metamask
                    </div>
                  </Media>
                  <Media lessThan="xxl">
                    <div
                      id="metamask"
                      className="flex justify-between "
                      onClick={logInMetamask}
                    >
                      <Image
                        src={"/images/metamask.png"}
                        width={21}
                        height={20}
                        alt="metamask-icon"
                        draggable="false"
                      />
                      Metamask
                    </div>
                  </Media>
                </a>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }: any) => (
                <a
                  className={classNames(
                    active ? "bg-gray-100 text-gray-900" : "text-gray-700",
                    "block px-4 py-2 text-sm 4K:text-3xl"
                  )}
                >
                  <Media greaterThanOrEqual="xxl">
                    <div
                      className="flex justify-between"
                      onClick={logInCoinbase}
                    >
                      <Image
                        src={"/images/coinbase.png"}
                        width={37}
                        height={25}
                        alt="coinbase-icon"
                        draggable="false"
                      />
                      Coinbase
                    </div>
                  </Media>
                  <Media lessThan="xxl">
                    <div
                      className="flex justify-between"
                      onClick={logInCoinbase}
                    >
                      <Image
                        src={"/images/coinbase.png"}
                        width={22}
                        height={20}
                        alt="coinbase-icon"
                        draggable="false"
                      />
                      Coinbase
                    </div>
                  </Media>
                </a>
              )}
            </Menu.Item>
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
