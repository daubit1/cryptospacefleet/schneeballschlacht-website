import { useState } from "react";

interface CircleProps {
  titelFormated: number | string;
  titel: string;
  text: string;
  className: string;
  classNameTitelFormated: string;
}

const Circle = ({
  titel,
  titelFormated,
  text,
  className,
  classNameTitelFormated,
}: CircleProps) => {
  const [show, setShow] = useState(false);
  console.log({ text, titel, titelFormated });
  return (
    <div className={className}>
      <div className="lg:text-md relative flex flex-col justify-center text-[8px] text-[#76f3ff] md:text-sm xl:text-lg 2xl:text-xl 3xl:text-2xl 4K:text-4xl">
        <div
          className={classNameTitelFormated}
          style={
            titelFormated < 1000 ? { cursor: "auto" } : { cursor: "pointer" }
          }
          onClick={() => setShow(!show)}
        >
          {show &&
            (titelFormated < 1000 ? null : (
              <div className="titel">{titel}</div>
            ))}
          {titelFormated}
        </div>
        <div>{text}</div>
      </div>
    </div>
  );
};
export default Circle;
