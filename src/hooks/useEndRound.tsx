import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { isFinished, isPaused } from "../lib/getContractData";
import useContract from "./useContract";
import { toast } from "react-toastify";
import { setFinished } from "../state/finished";
import { setPaused } from "../state/paused";

export default function useEndRound() {
  const { contract } = useContract();
  const dispatch = useDispatch();
  const endRound = useCallback(async () => {
    if (contract) {
      try {
        const _finished = await isFinished(contract);
        const _paused = await isPaused(contract);
        if (_finished && _paused) {
          const endRoundtsx = await contract["endRound()"]();
          console.log("tosstsx", endRoundtsx);
          const wait = endRoundtsx.wait();
          console.log(await wait);
          toast.promise(wait, {
            pending: "🥚 Transaction is pending!",
            success: `🦅 Snowball tossed!`,
            error: "🍳 Failed to send transaction!",
          });
        } else {
          toast("You cannot end the round yet");
        }
        dispatch(setFinished(_finished));
        dispatch(setPaused(_paused));
      } catch (e) {
        let msg = "Failed to send transaction!";
        toast.error(msg, {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          draggable: true,
          progress: undefined,
        });
        console.log(e);
      }
    }
  }, [contract, dispatch]);

  return endRound;
}
