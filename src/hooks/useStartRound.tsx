import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { sleep } from "../lib/functions";
import { setFinished } from "../state/finished";
import { setPaused } from "../state/paused";
import useContract from "./useContract";
import useRefreshData from "./useRefreshData";

export default function useStartRound(account: string) {
  const { contract } = useContract();
  const dispatch = useDispatch();
  const LogIn = useRefreshData(account, "latest");

  const startRound = useCallback(async () => {
    if (contract) {
      try {
        const paused: boolean = await contract["paused()"]();
        const finished: boolean = await contract["finished()"]();
        if (paused && !finished) {
          const tx = await contract["startRound()"]();
          const txResult = await tx.wait();
          console.log(txResult);
          while (true) {
            await sleep(1000);
            const paused: boolean = await contract["paused()"]();
            if (!paused) {
              await LogIn();
              break;
            }
          }
        } else {
          await LogIn();
        }
        dispatch(setFinished(finished));
        dispatch(setPaused(paused));
      } catch (e) {
        console.log(e);
      }
    }
  }, [LogIn, contract, dispatch]);

  return startRound;
}
