import { BigNumber } from "ethers";
import { useEffect, useState } from "react";
import { Network } from "../lib/network";
import { useViewContract } from "./useViewContract";

export default function useCurrentRound(chainId: Network) {
  const contract = useViewContract(chainId);
  const [round, setRound] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (contract) {
          const r: BigNumber = await contract["getRoundId"]();
          setRound(r.toNumber());
        }
      } catch (e) {
        console.log(e);
      }
    };
    fetchData();
  }, [contract]);

  return round;
}
