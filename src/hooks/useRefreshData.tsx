import { useCallback } from "react";
import { useDispatch } from "react-redux";
import {
  getFriendsList,
  getLeaderBoard,
  getRoundStatistik,
  getTotalStatistik,
  isFinished,
  isPaused,
} from "../lib/getContractData";
import { setFinished } from "../state/finished";
import { setPaused } from "../state/paused";
import useContract from "./useContract";

export default function useRefreshData(
  account: string,
  round: number | "latest"
) {
  const dispatch = useDispatch();
  const { contract, provider } = useContract();
  const LogIn = useCallback(async () => {
    if (contract) {
      try {
        const paused = await isPaused(contract);
        const finished = await isFinished(contract);
        dispatch(setPaused(paused));
        dispatch(setFinished(finished));
        if (!paused) {
          // dispatches get auto batched by react >= 18
          //Total Statisitk
          dispatch(await getTotalStatistik(contract, provider));
          //Round Statistik
          console.log(account);
          const {
            totalCurrentToken,
            snowballsTossed,
            statsSnowballs,
            statsRoundTotal,
          } = await getRoundStatistik(contract, provider, account, round);
          dispatch(statsSnowballs);
          dispatch(statsRoundTotal);
          //Leaderboard
          const { accountTokenIds, arrPlayer, statsLeaderBoard } =
            getLeaderBoard(totalCurrentToken, snowballsTossed, account);
          dispatch(statsLeaderBoard);
          if (round !== "latest") {
            //Friendsboard
            dispatch(
              await getFriendsList(
                accountTokenIds,
                contract,
                totalCurrentToken,
                arrPlayer
              )
            );
          }
        }
      } catch (e) {
        console.log(e);
      }
    }
  }, [contract, dispatch, provider, account, round]);
  return LogIn;
}
