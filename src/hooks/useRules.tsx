import { useTranslation } from "next-i18next";
import { DataRule } from "../types/Rules";

export const useRules = (): DataRule[] => {
  const { t } = useTranslation();
  return [
    {
      id: 1,
      titel: `${t("gameRules.text.titel")}`,
      text1: `${t("gameRules.text.text")}`,
      text2: "",
    },
    {
      id: 2,
      titel: `${t("gameRules.text.titel1")}`,
      text1: `${t("gameRules.text.text1")}`,
      text2: "",
    },
    {
      id: 3,
      titel: `${t("gameRules.text.titel2")}`,
      text1: `${t("gameRules.text.text2.1")}`,
      text2: `${t("gameRules.text.text2.2")}`,
    },
    {
      id: 4,
      titel: `${t("gameRules.text.titel3")}`,
      text1: `${t("gameRules.text.text3")}`,
      text2: "",
    },
    {
      id: 5,
      titel: `${t("gameRules.text.titel4")}`,
      text1: `${t("gameRules.text.text4")}`,
      text2: "",
    },
    {
      id: 6,
      titel: `${t("gameRules.text.titel5")}`,
      text1: `${t("gameRules.text.text5")}`,
      text2: "",
    },
    // {
    //   id: 7,
    //   titel: `${t("gameRules.text.titel6")}`,
    //   text1: `${t("gameRules.text.text6")}`,
    //   text2: "",
    // },
  ];
};
