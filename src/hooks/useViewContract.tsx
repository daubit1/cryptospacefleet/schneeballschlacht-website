import { useMemo } from "react";
import { ethers, providers } from "ethers";
import { CONTRACT_ABI } from "../contracts/abi";
import { Network } from "../lib/network";
import { getRPC } from "../lib/functions";
import { CONTRACT_ADDR } from "../lib/const";

export const useViewContract = (chainId: Network) => {
  return useMemo(() => {
    const provider = new providers.JsonRpcProvider(getRPC(chainId), chainId);
    const contract = new ethers.Contract(CONTRACT_ADDR, CONTRACT_ABI, provider);
    return contract;
  }, [chainId]);
};
