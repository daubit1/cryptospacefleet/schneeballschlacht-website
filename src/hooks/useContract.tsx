import { useWeb3React } from "@web3-react/core";
import { Contract } from "ethers";
import { useEffect, useState } from "react";
import { CONTRACT_ABI } from "../contracts/abi";
import { CONTRACT_ADDR } from "../lib/const";

export default function useContract() {
  const context = useWeb3React();
  const [contract, setContract] = useState<Contract | null>();
  useEffect(() => {
    try {
      const provider = context.provider;
      if (provider) {
        const signer = provider.getSigner();
        const contract = new Contract(CONTRACT_ADDR, CONTRACT_ABI, provider);
        setContract(contract.connect(signer));
      }
    } catch (e) {
      throw e;
    }
  }, [context.provider]);

  return { contract, provider: context.provider! };
}
