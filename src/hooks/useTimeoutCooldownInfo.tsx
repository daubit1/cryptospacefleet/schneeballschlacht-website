import { useCallback } from "react";
import { Network } from "../lib/network";
import { useViewContract } from "./useViewContract";

export default function useTimeoutCooldownInfo(chainId: Network) {
  const viewContract = useViewContract(chainId);
  const cooldown = useCallback(
    (address: string) => {
      return viewContract["onCooldownTill(address)"](address);
    },
    [viewContract]
  );
  const timeout = useCallback(
    (address: string) => {
      return viewContract["timedOutTill(address)"](address);
    },
    [viewContract]
  );

  return [timeout, cooldown];
}
