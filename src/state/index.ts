import { configureStore, combineReducers } from "@reduxjs/toolkit";
import statTotal from "./statsTotalSlice";
import statsTotalRound from "./statsTotalRoundSlice";
import statsLeader from "./statsLeaderBoardSlice";
import statsFriends from "./statsFriendsBoardSlice";
import statsSnowballs from "./statsSnowballsSlice";
import paused from "./paused";
import finished from "./finished";
//import {
//  persistStore,
//  persistReducer,
//  FLUSH,
//  REHYDRATE,
//  PAUSE,
//  PERSIST,
//  PURGE,
//  REGISTER,
//} from "redux-persist";
//import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

const rootReducers = combineReducers({
  statTotal,
  statsTotalRound,
  statsLeader,
  statsFriends,
  statsSnowballs,
  paused,
  finished,
});

//const persistConfig = {
//  key: "root",
//  storage,
//};

//const persistedReducer = persistReducer(persistConfig, rootReducers);

const store = configureStore({
  reducer: rootReducers,
  /*middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),*/
});

//export const persistor = persistStore(store);
export default store;
export type AppState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
