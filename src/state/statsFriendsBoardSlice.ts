import { createSlice } from "@reduxjs/toolkit";
import { StatsBoard } from "./statsLeaderBoardSlice";

export interface FriendsBoard {
  friends: StatsBoard[];
}

const initialState: FriendsBoard = {
  friends: [],
};

export const statsFriendsBoard = createSlice({
  name: "statsFriendsBoard",
  initialState,
  reducers: {
    setStatsFriendsBoard: (
      state,
      action: {
        payload: StatsBoard[];
      }
    ) => {
      state.friends = action.payload;
    },
    addToFriendsBoard: (
      state,
      action: { payload: { addr: string; level: number } }
    ) => {
      const friend = state.friends.findIndex(
        (x) => x.playerBoard === action.payload.addr
      );
      if (friend === -1) {
        state.friends.push({
          playerBoard: action.payload.addr,
          levelBoard: action.payload.level,
          tossedSnowballBoard: 0,
        });
      } else {
        if (state.friends[friend].levelBoard < action.payload.level) {
          state.friends[friend].levelBoard = action.payload.level;
        }
      }
    },
  },
});
export const { setStatsFriendsBoard, addToFriendsBoard } =
  statsFriendsBoard.actions;

export default statsFriendsBoard.reducer;
