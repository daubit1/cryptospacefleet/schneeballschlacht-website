import { createSlice } from "@reduxjs/toolkit";

export interface StatsSnowball {
  snowball: number;
  levelSnowball: number;
  partnerCount: number;
}
export interface Snowballs {
  snowballs: StatsSnowball[];
}

const initialState: Snowballs = {
  snowballs: [],
};

export const statsSnowballsSlice = createSlice({
  name: "statsSnowballs",
  initialState,
  reducers: {
    setStatsSnowballs: (
      state,
      action: {
        payload: StatsSnowball[];
      }
    ) => {
      state.snowballs = action.payload;
    },
    mintSnowball: (
      state,
      action: { payload: { id: number; level: number } }
    ) => {
      state.snowballs.push({
        snowball: action.payload.id,
        levelSnowball: action.payload.level,
        partnerCount: 0,
      });
    },
    tossSnowball: (state, action: { payload: number }) => {
      const index = state.snowballs.findIndex(
        (x) => x.snowball === action.payload
      );
      if (index > -1) {
        state.snowballs[index] = {
          ...state.snowballs[index],
          partnerCount: state.snowballs[index].partnerCount + 1,
        };
      }
    },
  },
});

export const { setStatsSnowballs, mintSnowball, tossSnowball } =
  statsSnowballsSlice.actions;

export default statsSnowballsSlice.reducer;
