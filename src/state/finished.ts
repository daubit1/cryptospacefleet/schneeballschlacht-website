import { createSlice } from "@reduxjs/toolkit";

export interface FinishedState {
  finished: boolean;
}

const initialState: FinishedState = {
  finished: false,
};

export const finished = createSlice({
  name: "finished",
  initialState,
  reducers: {
    setFinished: (
      state,
      action: {
        payload: boolean;
      }
    ) => {
      state.finished = action.payload;
    },
  },
});
export const { setFinished } = finished.actions;

export default finished.reducer;
