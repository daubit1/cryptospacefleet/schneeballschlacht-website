import { createSlice } from "@reduxjs/toolkit";

export interface PausedState {
  paused: boolean;
}

const initialState: PausedState = {
  paused: true,
};

export const paused = createSlice({
  name: "paused",
  initialState,
  reducers: {
    setPaused: (
      state,
      action: {
        payload: boolean;
      }
    ) => {
      state.paused = action.payload;
    },
  },
});
export const { setPaused } = paused.actions;

export default paused.reducer;
