import { createSlice } from "@reduxjs/toolkit";
import { utils } from "ethers";
import { MINT_FEE } from "../lib/const";

export interface StatsTotal {
  round: number;
  tosses: number;
  money: number;
}

const initialState: StatsTotal = {
  round: 0,
  tosses: 0,
  money: 0,
};

export const statsTotalSlice = createSlice({
  name: "statsTotal",
  initialState,
  reducers: {
    setStatsTotal: (
      state,
      action: { payload: { round: number; tosses: number; money: number } }
    ) => {
      state.round = action.payload.round;
      state.tosses = action.payload.tosses;
      state.money = action.payload.money;
    },
    mintTokenTotal: (state) => {
      console.log(+utils.formatEther(MINT_FEE));
      console.log(state.money);
      //state.money = state.money + +utils.formatEther(MINT_FEE);
    },
    tossTokenTotal: (state) => {
      state.tosses += 1;
    },
  },
});

export const { setStatsTotal, mintTokenTotal, tossTokenTotal } =
  statsTotalSlice.actions;

export default statsTotalSlice.reducer;
