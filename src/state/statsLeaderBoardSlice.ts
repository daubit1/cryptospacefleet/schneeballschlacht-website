import { createSlice } from "@reduxjs/toolkit";

export interface StatsBoard {
  playerBoard: string;
  levelBoard: number;
  tossedSnowballBoard: number;
}

export interface LeaderBoard {
  player: StatsBoard[];
}

const initialState: LeaderBoard = {
  player: [],
};

export const statsLeaderBoard = createSlice({
  name: "statsLeaderBoard",
  initialState,
  reducers: {
    setStatsLeaderBoard: (
      state,
      action: {
        payload: StatsBoard[];
      }
    ) => {
      state.player = action.payload;
    },
    addToLeaderBoard: (
      state,
      action: { payload: { addr: string; level: number } }
    ) => {
      const player = state.player.findIndex(
        (x) => x.playerBoard === action.payload.addr
      );
      if (player === -1) {
        state.player.push({
          playerBoard: action.payload.addr,
          levelBoard: action.payload.level,
          tossedSnowballBoard: 0,
        });
      } else {
        if (state.player[player].levelBoard < action.payload.level) {
          state.player[player].levelBoard = action.payload.level;
        }
      }
    },
  },
});
export const { setStatsLeaderBoard, addToLeaderBoard } =
  statsLeaderBoard.actions;

export default statsLeaderBoard.reducer;
