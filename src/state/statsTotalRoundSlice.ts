import { createSlice } from "@reduxjs/toolkit";
import { utils } from "ethers";
import { MINT_FEE, TOSS_FEE } from "../lib/const";

export interface StatsRoundTotal {
  currentHighestLevel: number;
  players: number;
  stones: number;
  tosses: number;
  money: number;
  snowballs: number;
}

const initialState: StatsRoundTotal = {
  currentHighestLevel: 0,
  players: 0,
  stones: 0,
  tosses: 0,
  money: 0,
  snowballs: 0,
};

export const statsTotalRoundSlice = createSlice({
  name: "statsRoundTotal",
  initialState,
  reducers: {
    setStatsRoundTotal: (
      state,
      action: {
        payload: {
          currentHighestLevel: number;
          players: number;
          stones: number;
          tosses: number;
          money: number;
          snowballs: number;
        };
      }
    ) => {
      state.currentHighestLevel = action.payload.currentHighestLevel;
      state.players = action.payload.players;
      state.stones = action.payload.stones;
      state.tosses = action.payload.tosses;
      state.money = action.payload.money;
      state.snowballs = action.payload.snowballs;
    },
    mintTokenRoundTotal: (state) => {
      state.snowballs += 1;
      state.money = state.money + +utils.formatEther(MINT_FEE);
    },
    mintTokenPlayerRoundTotal: (state) => {
      state.players += 1;
    },
    tossTokenRoundTotal: (
      state,
      action: { payload: { level: number; hasStone: boolean } }
    ) => {
      state.snowballs += 1;
      state.tosses += 1;
      state.stones += action.payload.hasStone ? 1 : 0;
      state.money =
        state.money + +utils.formatEther(TOSS_FEE) * action.payload.level;
    },
  },
});
export const {
  setStatsRoundTotal,
  mintTokenRoundTotal,
  tossTokenRoundTotal,
  mintTokenPlayerRoundTotal,
} = statsTotalRoundSlice.actions;

export default statsTotalRoundSlice.reducer;
