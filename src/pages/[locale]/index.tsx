import type { NextPage } from "next";
import Head from "next/head";
import PlayerTable from "../../components/PlayerTable/PlayerTable";
import LanguageSwitcher from "../../components/LanguageSwitcher/LanguageSwitcher";
import Cookie from "../../components/Cookie/Cookie";
import SocialMedia from "../../components/SocialMedia/SocialMedia";
import Banner from "../../components/Banner/Banner";
import GifAnimation from "../../components/GifAnimation/GifAnimation";
import { getStaticPaths, makeStaticProps } from "../../lib/getStatic";
import RoundInfo from "../../components/RoundInfo/RoundInfo";
import GameInfo from "../../components/GameInfo/GameInfo";
import { useWeb3React } from "@web3-react/core";
import { useTranslation } from "next-i18next";
import LoginForm from "../../components/Access/LoginForm";
import { Spinner } from "../../components/Spinner/Spinner";
import { useCallback, useState } from "react";
import { metaMask } from "../../components/connectors/metamask";
import { coinbaseWallet } from "../../components/connectors/coinbase";
import { useRouter } from "next/router";
import GameRules from "../../components/GameRules/GameRules";
import SnowballSelection from "../../components/SnowballSelection/SnowballSelection";
import { Network } from "../../lib/network";
import { useSelector } from "react-redux";
import { AppState } from "../../state";
import NoRound from "../../components/NoRound";

const getStaticProps = makeStaticProps(["common", "footer"]);
export { getStaticPaths, getStaticProps };
function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
const Home: NextPage = () => {
  const { player } = useSelector((state: AppState) => state.statsLeader);
  const { friends } = useSelector((state: AppState) => state.statsFriends);
  const paused = useSelector((state: AppState) => state.paused.paused);
  const context = useWeb3React();
  const router = useRouter();
  const { t, i18n } = useTranslation();
  const [loading, setLoading] = useState(false);
  const deactivate = useCallback(async () => {
    if (metaMask) await metaMask.resetState();
    if (coinbaseWallet) {
      await coinbaseWallet.resetState();
      coinbaseWallet.deactivate();
    }
    setLoading(true);
    await delay(1000);
    router.push("/");
  }, [router]);

  return (
    <div className="mx-auto my-0 sm:w-[55%]">
      <Head>
        <title>Schneeballschlacht</title>
        <meta name="description" content="Schneeballschlacht" />
        <meta name="robots" content="index, follow" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Schneeballschlacht" />
        <meta property="og:description" content="Schneeballschlacht" />
        <meta property="og:image" content="/android-chrome-512x512.png" />
        <meta
          property="og:url"
          content={`https://schneeballschlacht.daubitprojectcore.com/${i18n.language}`}
        />
        <meta property="og:locale" content={i18n.language} />
        {i18n.languages.map((x) => (
          <meta key={x} property="og:locale:alternate" content={x} />
        ))}
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />
      </Head>
      <main>
        {loading ? (
          <Spinner />
        ) : (
          <>
            <LanguageSwitcher />
            <Banner logout={context.isActive} deactivate={deactivate} />
            <GifAnimation />
            <LoginForm
              network={Network.evmosTestnet}
              deactivate={deactivate}
              round="latest"
            />
            <SocialMedia />
            {context.isActive ? (
              paused ? (
                <>
                  <NoRound />
                  <GameRules />
                </>
              ) : (
                <>
                  <GameInfo />
                  <RoundInfo />
                  <SnowballSelection
                    state={friends}
                    title={t("statistik.titel4")}
                    bg={false}
                    account={context.account}
                    chainId={Network.evmosTestnet}
                  />
                  <PlayerTable
                    state={player}
                    title={t("statistik.titel3")}
                    bg={true}
                    cursor=""
                    selectedFriend=""
                  />
                  <GameRules />
                </>
              )
            ) : (
              <GameRules />
            )}
            <Cookie />
          </>
        )}
      </main>
    </div>
  );
};

export default Home;
