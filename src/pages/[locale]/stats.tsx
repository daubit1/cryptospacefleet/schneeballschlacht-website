import { useWeb3React } from "@web3-react/core";
import { NextPage } from "next";
import { useTranslation } from "next-i18next";
import Head from "next/head";
import { useRouter } from "next/router";
import { ChangeEvent, useCallback, useState } from "react";
import { useSelector } from "react-redux";
import LoginForm from "../../components/Access/LoginForm";
import Banner from "../../components/Banner/Banner";
import { coinbaseWallet } from "../../components/connectors/coinbase";
import { metaMask } from "../../components/connectors/metamask";
import Cookie from "../../components/Cookie/Cookie";
import GameInfo from "../../components/GameInfo/GameInfo";
import LanguageSwitcher from "../../components/LanguageSwitcher/LanguageSwitcher";
import PlayerTable from "../../components/PlayerTable/PlayerTable";
import RoundInfo from "../../components/RoundInfo/RoundInfo";
import { Spinner } from "../../components/Spinner/Spinner";
import useCurrentRound from "../../hooks/useCurrentRound";
import { getStaticPaths, makeStaticProps } from "../../lib/getStatic";
import { Network } from "../../lib/network";
import { AppState } from "../../state";

function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const getStaticProps = makeStaticProps(["common", "footer"]);
export { getStaticPaths, getStaticProps };

const StatsPage: NextPage = () => {
  const { t, i18n } = useTranslation();
  const context = useWeb3React();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const deactivate = useCallback(async () => {
    if (metaMask) await metaMask.resetState();
    if (coinbaseWallet) {
      await coinbaseWallet.resetState();
      coinbaseWallet.deactivate();
    }
    setLoading(true);
    await delay(1000);
    router.push("/");
  }, [router]);
  const { player } = useSelector((state: AppState) => state.statsLeader);
  const currentRound = useCurrentRound(Network.evmosTestnet);
  const [round, setRound] = useState(currentRound);

  function updateRound(e: ChangeEvent<HTMLInputElement>) {
    const { value } = e.target;
    try {
      const val = parseInt(value);
      setRound(Math.min(Math.max(1, val), currentRound));
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <div className="mx-auto my-0 sm:w-[55%]">
      <Head>
        <title>Schneeballschlacht</title>
        <meta name="description" content="Schneeballschlacht" />
        <meta name="robots" content="index, follow" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Schneeballschlacht" />
        <meta property="og:description" content="Schneeballschlacht" />
        <meta property="og:image" content="/android-chrome-512x512.png" />
        <meta
          property="og:url"
          content={`https://schneeballschlacht.daubitprojectcore.com/${i18n.language}`}
        />
        <meta property="og:locale" content={i18n.language} />
        {i18n.languages.map((x) => (
          <meta key={x} property="og:locale:alternate" content={x} />
        ))}
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />
      </Head>
      <main>
        {loading ? (
          <Spinner />
        ) : (
          <>
            <LanguageSwitcher />
            <Banner logout={context.isActive} deactivate={deactivate} />
            <LoginForm
              network={Network.evmosTestnet}
              deactivate={deactivate}
              round={round > 0 ? round : "latest"}
            />
            {!context.isActive || context.chainId !== Network.evmosTestnet ? null : (
              <section className="text mt-[4rem] flex flex-col justify-center bg-[#3A6274]">
                <div className="relative flex justify-center">
                  <div className="mt-[5rem] w-[65%] rounded-lg border-4 border-double border-[#76f3ff] text-right sm:w-[60%] lg:w-[50%] xl-l:mt-[8.5rem]">
                    <div className="p-[1rem] lg:p-[2rem]">
                      <div className="flex pb-[1rem]">
                        <span className="w-[50%] pr-[10%]">Current Round:</span>
                        <span className="w-[50%] pl-[2px] text-left">
                          {currentRound}
                        </span>
                      </div>
                      <div className="flex">
                        <label className="w-[50%] pr-[10%]" htmlFor="round">
                          Round:
                        </label>
                        <input
                          id="round"
                          value={round}
                          type="number"
                          onChange={(e) => updateRound(e)}
                          className="w-[50%] rounded border-2 border-solid border-transparent bg-[#3A6274] outline-none transition ease-in-out hover:border-2 hover:bg-[#3A6274] focus:border-2 focus:border-solid focus:border-[#76f3ff] focus:bg-[#3A6274]"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <GameInfo />
                <RoundInfo />
                <PlayerTable
                  state={player}
                  title={t("statistik.titel3")}
                  bg={true}
                  cursor=""
                  selectedFriend=""
                />
              </section>
            )}
            <Cookie />
          </>
        )}
      </main>
    </div>
  );
};

export default StatsPage;
