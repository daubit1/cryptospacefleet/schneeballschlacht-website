import "../styles/globals.css";
import "./App.scss";
import { reportWebVitals, sendToAnalytics } from "../reportWebVitals";
import type { AppProps } from "next/app";
import { appWithTranslation } from "next-i18next";
import { ParallaxProvider } from "react-scroll-parallax";
import { MetaMask } from "@web3-react/metamask";
import { Web3ReactHooks, Web3ReactProvider } from "@web3-react/core";
import { CoinbaseWallet } from "@web3-react/coinbase-wallet";
import {
  metaMask,
  hook as metaMaskHooks,
} from "../components/connectors/metamask";
import {
  coinbaseWallet,
  hooks as coinbaseWalletHooks,
} from "../components/connectors/coinbase";
import { getCookieConsentValue } from "react-cookie-consent";
import { useEffect } from "react";
import { Provider } from "react-redux";
//import { PersistGate } from "redux-persist/integration/react";
import GAnalytics from ".././Analytics";
import store from "../state";
import { ToastContainer } from "react-toastify";
import { MediaContextProvider } from "../utils/media";

const connectors: [MetaMask | CoinbaseWallet, Web3ReactHooks][] = [
  [metaMask, metaMaskHooks],
  [coinbaseWallet, coinbaseWalletHooks],
];

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    const isCookieStatisticsOn = getCookieConsentValue();
    if (isCookieStatisticsOn === "true") {
      GAnalytics.initialize();
    }
  }, []);

  return (
    <Provider store={store}>
      {/*<PersistGate loading={null} persistor={persistor}>*/}
      <Web3ReactProvider connectors={connectors}>
        <ParallaxProvider>
          <MediaContextProvider>
            <Component {...pageProps} />
            <ToastContainer
              position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={true}
              closeOnClick
              rtl={false}
              draggable
            />
          </MediaContextProvider>
        </ParallaxProvider>
      </Web3ReactProvider>
      {/*</PersistGate>*/}
    </Provider>
  );
}

typeof window !== "undefined" && reportWebVitals(sendToAnalytics);

export default appWithTranslation(MyApp);
