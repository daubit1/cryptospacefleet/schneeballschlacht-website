import { makeStaticProps } from "../lib/getStatic";
import { Redirect } from "../lib/redirect";
import Home from "./[locale]";

const getStaticProps = makeStaticProps(["common", "footer"], "de");
export { getStaticProps };

export default function RedirectHome() {
  return (
    <>
      <Home />
      <Redirect />
    </>
  );
}
