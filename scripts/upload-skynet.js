const {
  SkynetClient,
  getEntryLink,
  genKeyPairFromSeed,
} = require("@skynetlabs/skynet-nodejs");

const client = new SkynetClient();

(async () => {
  const seed = process.env["SKYNET_SEED"];
  const dataKey = process.env["SKYNET_DATAKEY"];
  if (!seed) {
    console.error("Cannot read seed value");
    process.exit(-1);
  }
  const { publicKey, privateKey } = genKeyPairFromSeed(seed);
  const skylink = await client.uploadDirectory("./out");
  console.log(`Upload successful, skylink: ${skylink}`);
  // set a registry entry to point at 'skylink'
  await client.db.setDataLink(privateKey, dataKey, skylink);

  // get the resolver skylink which references the registry entry
  const resolverSkylink = getEntryLink(publicKey, dataKey);
  console.log(`Pointing skylink to ${resolverSkylink}`);
})();
