#!/usr/bin/env rust-script
//! ```cargo
//! [dependencies]
//! build-tools-lib = { git = "https://gitlab.com/daubit1/build-tools", rev = "1.5.1"}
//! clap = {version = "3.1.18", features=["derive"]}
//! semver = "1.0.9"
//! serde = "1.0.144"
//! serde_yaml = "0.9.11"
//! ```
use std::{
    fs::{write, File},
    io::{Error as IOError, ErrorKind, Read},
    path::Path,
};

use build_tools_lib::*;
use clap::Parser;
use semver::Version;
use serde::{Deserialize, Serialize};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct DefaultArgs {
    #[clap(short, long)]
    quiet: bool,
    #[clap(short, long)]
    env: Env,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Image {
    pub image: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Services {
    pub web: Option<Image>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Config {
    pub services: Option<Services>,
}

fn read_file(file: &Path) -> Result<String, IOError> {
    let mut file = File::open(file)?;
    let mut raw_data = String::new();
    file.read_to_string(&mut raw_data)?;
    Ok(raw_data)
}

fn update_image(yaml_file: &Path, new_image: &String) -> Result<(), IOError> {
    let file = read_file(yaml_file)?;
    let deployment = serde_yaml::from_str(&file);
    if deployment.is_err() {
        return Err(IOError::new(ErrorKind::NotFound, "deploy not found").into());
    }
    let config: Config = deployment.unwrap();
    let services = config.services;
    if services.is_none() {
        return Err(IOError::new(ErrorKind::NotFound, "services not found").into());
    }
    let web = services.unwrap().web;
    if web.is_none() {
        return Err(IOError::new(ErrorKind::NotFound, "web not found").into());
    }
    let old_image = web.unwrap().image;
    if old_image.is_none() {
        return Err(IOError::new(ErrorKind::NotFound, "image not found").into());
    }
    let old_image = old_image.unwrap();
    let file = file.replace(&old_image, &new_image);
    return write(yaml_file, file);
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const VERSION: &str = "1.0.2";
    println!("Uploading version: {VERSION}");
    let semversion = &Version::parse(VERSION)?;

    let args = DefaultArgs::parse();

    let h = BuildToolHelper::new(args.quiet, args.env);

    h.update_json_version(Path::new("./package.json"), semversion)?;

    let local_tag = h.docker_compose_build("web")?;
    let remote_tag = h.docker_tag(&local_tag, "counter11/schneeballschlacht-", "web", VERSION)?;
    h.docker_push(&remote_tag)?;

    update_image(Path::new("./deploy.yaml"), &remote_tag)?;

    h.git_add_all()?;
    h.git_commit(VERSION)?;
    h.git_tag(VERSION)?;
    h.git_push()?;
    h.git_push_tag(VERSION)?;

    Ok(())
}
