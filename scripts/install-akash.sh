if [[ -z $(type -p akash) ]]; then
    TMP=$PWD
    cd ~
    echo "Fetching akash..."
    AKASH_VERSION="$(curl -s "https://raw.githubusercontent.com/ovrclk/net/master/mainnet/version.txt")"
    curl -s https://raw.githubusercontent.com/ovrclk/akash/master/godownloader.sh | sh -s -- "v$AKASH_VERSION"
    echo "akash version: $($HOME/bin/akash version)"
    cd $TMP

else
    echo "Akash already installed, continuing..."
fi