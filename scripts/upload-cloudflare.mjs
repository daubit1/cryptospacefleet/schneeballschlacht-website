import fetch from "node-fetch";
import dotenv from "dotenv";
dotenv.config({ path: ".env.local" });

const ZONE_URL = "https://api.cloudflare.com/client/v4/zones";
const GET_DNS_RECORD_URL = (zoneId, domain) =>
  `https://api.cloudflare.com/client/v4/zones/${zoneId}/dns_records?name=${domain}&type=CNAME`;

const UPDATE_DNS_RECORD_URL = (zoneId, dnsId) =>
  `https://api.cloudflare.com/client/v4/zones/${zoneId}/dns_records/${dnsId}`;

async function main() {
  const AUTH_EMAIL = process.env["AUTH_EMAIL"];
  const AUTH_KEY = process.env["AUTH_KEY"];
  const uri = process.argv[2];
  if (!AUTH_EMAIL || !AUTH_KEY || !uri) {
    console.log("Missing values");
    process.exit(-1);
  }
  const headers = {
    "X-Auth-Key": AUTH_KEY,
    "X-Auth-Email": AUTH_EMAIL,
    "Content-Type": "application/json",
  };
  let resp = await fetch(ZONE_URL, {
    headers,
  });
  const zones = await resp.json();
  const zoneId = zones.result[0].id;
  const domain = `sbs.${zones.result[0].name}`;
  resp = await fetch(GET_DNS_RECORD_URL(zoneId, domain), {
    headers,
  });
  const result = await resp.json();
  const dns = result.result[0];
  // console.log({ result, dns });
  if (!dns) {
    console.log("Couldnt fetch DNS");
    process.exit(-1);
  }
  const body = {
    type: "CNAME",
    name: domain,
    content: uri,
    ttl: 1,
    proxied: true,
  };
  resp = await fetch(UPDATE_DNS_RECORD_URL(zoneId, dns.id), {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
  });
  resp = await resp.json();
  console.log(`Linking was ${resp.success ? "succesfull": "unsuccessful"}`);
}

main();
