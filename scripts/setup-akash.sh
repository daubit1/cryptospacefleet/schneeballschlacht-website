if [[ -z $AKASH_KEY_NAME ]]; then
    echo "Missing key name"
    exit -1
fi

AKASH_KEY_NAME=$(sed -e 's,\\[trn],,g' <<<"$AKASH_KEY_NAME")

if [[ -z $($HOME/bin/akash keys list -n) ]]; then
    echo "Importing new account..."
    $HOME/bin/akash keys import $AKASH_KEY_NAME scripts/keyring.txt
else
    echo "Already imported an account..."
fi

export AKASH_ACCOUNT_ADDRESS="$($HOME/bin/akash keys show $AKASH_KEY_NAME -a)"

if [[ -z $AKASH_ACCOUNT_ADDRESS ]]; then
    echo "Import failed..."
    exit -1
fi

echo "Deploying with $AKASH_ACCOUNT_ADDRESS"

AKASH_NET="https://raw.githubusercontent.com/ovrclk/net/master/mainnet"
AKASH_VERSION="$(curl -s "$AKASH_NET/version.txt")"
AKASH_CHAIN_ID="$(curl -s "$AKASH_NET/chain-id.txt")"
export AKASH_NODE="$(curl -s "$AKASH_NET/rpc-nodes.txt" | shuf -n 1)"

if ! [[ -f "$HOME/.akash/$AKASH_ACCOUNT_ADDRESS.pem" ]]; then
    echo "Generating certificate..."
    $HOME/bin/akash tx cert generate client --from $AKASH_KEY_NAME --overwrite
    echo "Publishing certificate..."
    $HOME/bin/akash tx cert publish client --from $AKASH_KEY_NAME --chain-id $AKASH_CHAIN_ID -y --gas auto --fees 2500uakt
else
    echo "Certificate exists!"
fi

# akash query bank balances --node $AKASH_NODE $AKASH_ACCOUNT_ADDRESS
DEPLOYMENT=$($HOME/bin/akash query deployment list --node $AKASH_NODE --owner $AKASH_ACCOUNT_ADDRESS --output json)
DEPLOYMENT_STATE="$(jq -r '.deployments | .[-1] | .deployment.state' <<<"$DEPLOYMENT")"

if [[ $DEPLOYMENT_STATE == active ]]; then
    DSEQ=$(jq -r '.deployments | .[-1] | .deployment.deployment_id.dseq' <<<"$DEPLOYMENT")
    echo "DSEQ: $DSEQ"
    echo "Updating..."
    $HOME/bin/akash tx deployment update deploy.yaml --dseq $DSEQ --from $AKASH_KEY_NAME --chain-id $AKASH_CHAIN_ID
    AKASH_PROVIDER=$($HOME/bin/akash query market lease list --node $AKASH_NODE --owner $AKASH_ACCOUNT_ADDRESS --dseq $DSEQ --output json |
        jq -r '.leases | .[0].lease.lease_id.provider')
    $HOME/bin/akash provider send-manifest deploy.yaml --dseq $DSEQ --provider $AKASH_PROVIDER --from $AKASH_KEY_NAME
else
    echo "Deploying..."

    # Create deployment
    $HOME/bin/akash tx deployment create deploy.yaml --from $AKASH_KEY_NAME --chain-id $AKASH_CHAIN_ID -y --gas auto --fees 2500uakt

    # Fetch deployment data
    DEPLOYMENT=$($HOME/bin/akash query deployment list --node $AKASH_NODE --owner $AKASH_ACCOUNT_ADDRESS --output json)
    # echo $(jq -r . <<<"$DEPLOYMENT")
    DEPLOYMENT_STATE="$(jq -r '.deployments | .[-1] | .deployment.state' <<<"$DEPLOYMENT")"
    if [[ $DEPLOYMENT_STATE == closed ]]; then
        echo "Deployment not open!"
        exit -1
    fi

    DSEQ=$(jq -r '.deployments | .[-1] | .deployment.deployment_id.dseq' <<<"$DEPLOYMENT")
    echo "DSEQ: $DSEQ"

    # Fetch bids
    BIDS=$($HOME/bin/akash query market bid list --owner $AKASH_ACCOUNT_ADDRESS --node $AKASH_NODE --dseq $DSEQ --state open --output json --chain-id $AKASH_CHAIN_ID |
        jq -r '.bids')

    if [[ $BIDS == [] ]]; then
        echo "Create deployment failed"
        exit -1
    fi

    echo $BIDS

    AKASH_PROVIDER=$(jq -r '.[-1].bid.bid_id.provider' <<<"$BIDS")
    if [[ $AKASH_PROVIDER == null ]]; then
        echo "Could not select a provider!"
        exit -1
    fi

    echo "Provider: $AKASH_PROVIDER"
    # Check response "bid not open" -> close & re-open, "state: active" -> send-manifest
    $HOME/bin/akash tx market lease create --dseq $DSEQ --provider $AKASH_PROVIDER --from $AKASH_KEY_NAME --chain-id $AKASH_CHAIN_ID -y --gas auto --fees 2500uakt
    $HOME/bin/akash provider send-manifest deploy.yaml --dseq $DSEQ --provider $AKASH_PROVIDER --from $AKASH_KEY_NAME --chain-id $AKASH_CHAIN_ID
fi


# AKASH_PROVIDER=$($HOME/bin/akash query market lease list --node $AKASH_NODE --owner $AKASH_ACCOUNT_ADDRESS --dseq $DSEQ --output json |
#     jq -r '.leases | .[0].lease.lease_id.provider')

# echo "Provider address: $AKASH_PROVIDER"

URI=$($HOME/bin/akash provider lease-status --provider $AKASH_PROVIDER --node $AKASH_NODE --from $AKASH_KEY_NAME --dseq $DSEQ |
    jq -r '.services.web.uris | .[0]')

echo "Linking $URI"

if [[ -z $URI ]]; then
    echo "Error: Cannot get URI!"
    exit -1
fi

node scripts/upload-cloudflare.mjs $URI
