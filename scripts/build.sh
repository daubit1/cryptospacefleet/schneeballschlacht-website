#!/bin/bash
rm -r ./out
rm -r node_modules

yarn install --immutable --immutable-cache --check-cache
yarn export

find out -type f -name "*.css" -exec sh -c "gzip < '{}' > '{}'.gz" \;
find out -type f -name "*.js" -exec sh -c "gzip < '{}' > '{}'.gz" \;
find out -type f -name "*.html" -exec sh -c "gzip < '{}' > '{}'.gz" \;
find out -type f -name "*.eot" -exec sh -c "gzip < '{}' > '{}'.gz" \;
find out -type f -name "*.ttf" -exec sh -c "gzip < '{}' > '{}'.gz" \;
find out -type f -name "*.svg" -exec sh -c "gzip < '{}' > '{}'.gz" \;